# LGFM
This repository contains the code needed to replicate the results in the paper 
Latent Gaussian random field mixture models 
by David Bolin, Jonas Wallin, and Finn Lindgren. 

The code is provided as is. Note that no random seed is set for the simulated examples, so the numbers will not be exactly the same as in the article.


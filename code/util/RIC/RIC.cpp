/*

	An Robustifed incomplete Cholesky preconditioner 
	Based on algorithm RIC1 (with small modification) in [1],
	The actual implimenation is based heavly on [2]

	[1] Kaporin I,
	High Quality Preconditioning of a General Symmetric Positive Definite Matrix Based on its
	UT U + UT R + RT U-decomposition,  NUMERICAL LINEAR ALGEBRA WITH APPLICATIONS 1998
	[2] Chih-Jen Lin and Jorge Moré,
	 ICFS: An Incomplete Cholesky Factorization with Limited Memory
	http://www.mcs.anl.gov/~more/icfs/index.html


% Author: Jonas Wallin 
*/

//To compile on Linux:
//mex -largeArrayDims -O RIC.cpp
//debug: mex -g -DDEBUG -DTIMING -largeArrayDims RIC.cpp




#include "mex.h"
#include "matrix.h"
#include<cstdio>
#include<algorithm>
#include<list>
#include<vector>
#include<map>
#ifdef _OPENMP
#include<omp.h>
#endif
#include <cmath>
#ifdef TIMING
#include<ctime>
#endif

typedef std::map< size_t,double > Qvectype;
typedef std::vector< Qvectype > Qtype;

/**Checks if a given array contains a square, full, real valued, double matrix.
 *@param tmp pointer to a matlab array.*/
bool mxIsRealSparseDouble(const mxArray* tmp){
  return mxIsSparse(tmp) && !mxIsComplex(tmp) &&
    mxIsDouble(tmp) && !mxIsEmpty(tmp);
}
/**Checks if a given array contains a real positive scalar.
 *@param tmp pointer to a matlab array.*/
bool mxIsRealPosScalar(const mxArray* tmp){
  return mxGetNumberOfElements(tmp)==1 && !mxIsComplex(tmp) &&
    mxIsDouble(tmp) && !mxIsEmpty(tmp);
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]){

  if(nlhs>1)
    mexErrMsgTxt("Function returns ONE value.");
  if(nrhs!=1 && nrhs!=2 && nrhs!=3)
    mexErrMsgTxt("Function requires 1-3 input(s).");
  if(!mxIsRealSparseDouble(prhs[0]))
    mexErrMsgTxt("First argument should be a sparse, double matrix.");
    double tol = 1e-3;
  if(nrhs>1){
    if(!mxIsRealPosScalar(prhs[1]))
      mexErrMsgTxt("Second argument should be a real.");
    double *ptrData = mxGetPr(prhs[1]);
    if(ptrData==NULL)
      mexErrMsgTxt("Unable to obtain pointers to second argument.");

      tol = *ptrData;
      //mexPrintf("tol  = %lf\n",tol);
    }
    double diag_comp = 1; // how much to compensate diagonal with value in [0,1]
    if(nrhs==3){
        if(!mxIsRealPosScalar(prhs[2]))
            mexErrMsgTxt("Second argument should be a real.");
        double *ptrData = mxGetPr(prhs[2]);

        if(ptrData==NULL)
            mexErrMsgTxt("Unable to obtain pointers to second argument.");
        if( *ptrData < 0)
            mexErrMsgTxt("Third argument should be  in [0,1].");
        if( *ptrData > 1)
            mexErrMsgTxt("Third argument should be  [0,1].");

        diag_comp = *ptrData;
    }


	mwIndex *Rir = mxGetIr(prhs[0]);
	mwIndex *Rjc = mxGetJc(prhs[0]);
	double *Rpr = mxGetPr(prhs[0]);
	int nz = mxGetNzmax (prhs [0]);
	if(Rir==NULL || Rjc==NULL || Rpr==NULL)
		mexErrMsgTxt("Unable to obtain pointers to data in R-matrix.");
	int n =  mxGetN(prhs[0]);
	if(n != mxGetM(prhs[0]) )
		mexErrMsgTxt("R-matrix should be square");

    size_t nzmax_out = 0;

	/* the diagonal vector*/
	std::vector<double> D(n);
	std::vector<long unsigned int> D_pos(n); // position in vector
	for(int col = 0; col < n; col++)
	{
	    double diag_val = -1;
	    for(int row_pos = Rjc[col]; row_pos < Rjc[col+1]; row_pos++){
            if(Rir[row_pos] == col)
            {
                diag_val = Rpr[row_pos];
                D_pos[col] = row_pos;
                break;
            }

	    }
	    if( diag_val < 0)
            mexErrMsgTxt("EMPTY or NEGATIVE DIAGONAL THUS ERRROOROROROROROOROR.");

        D[col] = diag_val;

	}
    #ifdef TIMING
        // TIMING
        clock_t start = clock();
        const double ticks_per_ms = static_cast<double>(CLOCKS_PER_SEC)/1000;
        double init_time = static_cast<double>(clock()-start)  / ticks_per_ms;
        double time_step3 = 0;
        double time_step4 = 0;
        double time_step6 = 0;
        double time_step78 = 0;
    #endif


    int nz_true = 0;

    plhs[0] = mxCreateSparse(n,n,10*nz,mxREAL);

    double* pr = mxGetPr(plhs[0]);
    mwIndex* ir = mxGetIr(plhs[0]);
    mwIndex* jc = mxGetJc(plhs[0]);
    jc[0] = 0;
    int count = 0;


    std::vector<long int> v_pos(n);
    std::vector<double> v_vals(n);
    std::vector<long int> v_nonzero(n); //smaller values then col in loop
                                   // will represent largest value in use!
    std::vector<long int> chain_(n,-1); //previous elements, brilliant set of code coming here, not intutive for a statistician!
    std::vector<long int> col_end_ptr(n); // col_end_ptr(k + 1) pointer to last element in column k
    int v_count;
    for(int col = 0; col < n ; col ++)
    {
        v_count = 0;

        #ifdef TIMING
            start = clock();
        #endif
        /*  STEP 3 IN article*/
        // loops over all rows larger then column!
        for(int row_pos = D_pos[col]+1; row_pos < Rjc[col+1]; row_pos++){
            v_pos[v_count]  = Rir[row_pos];
            v_vals[v_pos[v_count]] = Rpr[row_pos];
            v_nonzero[v_pos[v_count]] = 1;
            //mexPrintf("v_vals[%d]= %lf\n",v_pos[v_count], v_vals[v_pos[v_count]] );
            v_count ++;


        }
        #ifdef TIMING
            time_step3 += static_cast<double>(clock()-start) / ticks_per_ms;
            start = clock();
        #endif
        long int k = -1;
        if( col != n-1)
            k = chain_[col];

        while( k != -1)
        {
            //mexPrintf("k= %d\n",k);
            long int start_row_k = v_nonzero[k]; //  largest  non zero value of column k
            //                                       with index larger then col!
            long int end_row_k = jc[k + 1];
            double u_k_col = pr[start_row_k];
            long int k_new = chain_[k];
            start_row_k += 1;

            if( start_row_k < end_row_k)
            {
                v_nonzero[k]            = start_row_k;
                chain_[k]               = chain_[ir[start_row_k]];
                chain_[ir[start_row_k]] = k;
            }

            for(int row = start_row_k; row < end_row_k; row++)
            {
                size_t row_index = ir[row];

                if( v_nonzero[row_index] ) // is v nonzero?
                    v_vals[row_index] -= u_k_col * pr[row];
                else
                {
                        v_nonzero[row_index] = 1;
                        v_pos[v_count]  = row_index;
                        v_count ++;
                        v_vals[row_index] = - u_k_col * pr[row];
                }

            }
            k = k_new;

        }

        // better sort?



        if(v_count > 0)
            std::sort (v_pos.begin(), v_pos.begin()+ v_count);


        #ifdef TIMING
            time_step4 += static_cast<double>(clock()-start) / ticks_per_ms;
            start = clock();
        #endif

        /*   step 6 in article */
        double d_col_sqrt = sqrt(D[col]);


        // TODO add reallocate
        if(jc[col] +v_count > 10*nz )
        {
            mexPrintf("size= %d\n",jc[col] +v_count);
            mexErrMsgTxt("error to large matrix\n");
        }

        ir[count] = col;
        count ++; // extra count for diagonal that we dont update yet

        for(int i = 0; i < v_count ; i++)
        {

            //mexPrintf("v_pos[%d] = %d\n",i ,v_pos[i]);
            double pos_i = v_pos[i];
            double v_i = v_vals[pos_i];
            if( D[pos_i] < 0)
                mexErrMsgTxt("negative piot encounted\n");
            double eta = fabs(v_i) /( d_col_sqrt * sqrt(D[pos_i]));
            //mexPrintf("eta ? tol = %lf ? %lf\n",eta,tol);
            if( eta < tol )
            {
                D[pos_i]      *= (1 + diag_comp * eta);
                D[col]        *= (1 + diag_comp * eta);
                d_col_sqrt     =  sqrt(D[col]);
                v_vals[pos_i] = 0;
            }
            else
            {
                ir[count] = pos_i;
                pr[count] = v_i;
                count++;
            }
        }
        //mexPrintf("*********** VPOS DONE *******************\n");
        jc[col + 1] = count;
        //mexPrintf("jc[%d] = %d\n",col,jc[col]);
        //mexPrintf("d_col_sqrt = %lf\n",d_col_sqrt);
        pr[jc[col]]  = d_col_sqrt; //update the diagonal
        for(int i = (jc[col] + 1); i < jc[col + 1]; i++ ){

            pr[i] /=  d_col_sqrt;
            D[ir[i]] -= pr[i]*pr[i];
        }

         #ifdef TIMING
            time_step6 += static_cast<double>(clock()-start) / ticks_per_ms;
            start = clock();
        #endif
        /* STEP 7-8 in algorithm*/






        /*

            Creating two smart chain used to update the values!
        */

        if(jc[col + 1] > (jc[col] + 1))
        {
                long unsigned int non_diag = ir[jc[col] + 1];
                v_nonzero[col]            = jc[col] + 1 ;
                chain_[col]               = chain_[non_diag];
                chain_[non_diag]       = col;
                //mexPrintf("v_nonzero[%d] = %d\n",col, non_diag);
                //mexPrintf("chain_[%d] = %d\n",non_diag, chain_[non_diag]);
                //mexPrintf("chain_[%d] = %d\n",col, chain_[col]);

        }
        for(int i = 0; i < v_count; i++)
            v_nonzero[v_pos[i]] = 0;
    }


    #ifdef TIMING
        mexPrintf("Timing info (in ms):\n");
        mexPrintf(" step 3 algorithm:  %7.1f\n",time_step3);
        mexPrintf(" step 6 algorithm:  %7.1f\n",time_step6);
        mexPrintf(" step 7-8 algorithm: %7.1f\n",time_step78);
        mexPrintf(" step 4 algorithm: %7.1f\n",time_step4);


    #endif
    //delete(&D);
    //delete(&U);
    //delete(&v);

}

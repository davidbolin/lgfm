clc 
clear all
n = 4;
if 0
    A = randn(n, n);
    A = A'*A + eye(size(A'*A,1)); 
    A = sparse(A);

else
    nnz = 0.5 * n;
    row = ceil(n*rand(ceil(nnz),1));
    col = ceil(n*rand(ceil(nnz),1));
    val = rand(ceil(nnz),1);
    A= sparse(row,col,val ,n ,n);
    A = (A+A')/2;
    A = A + speye(size(A)) + diag(sum(A));
end
full(A)
tic
L = RIC(A ,1);
toc
tic
L_c = chol(A)';
toc
max(max(L-L_c))
 full(L)
 full(L_c);

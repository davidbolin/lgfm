function L = RIC(A,tol,diag_adj)
%RIC1 - A robust incomplete cholesky
%
% drop elements A_{i,j}  if eta = |A_{i,j}| /sqrt{ L_{ii} * L_{jj}}  < tol
% then multiply L_{ii},L_{jj} (1+ diag_adj*eta). 
%
%
% use:
% L = RIC(A,tol = 1e-3, diag_adj = 1)
%
% input:
% 
%	A	 - (nxn) a sparse symmetric semi(?) - pos def matrix
%
%	tol	 - (double) drop tolerance
%
%	diag_adj - ([0,1]) how robust incomplete should be used, = 0 is regular IC, 
%								 = 1 can not break down from negative pivot 
%								     but the preconditioner is worse then the regular precond
%
%
% Based on algorithm RIC1 (with small modification) in  [1]
% [1] Kaporin I,
%	High Quality Preconditioning of a General Symmetric Positive Definite Matrix Based on its
%	UT U + UT R + RT U-decomposition,  NUMERICAL LINEAR ALGEBRA WITH APPLICATIONS 1998
% 
%

% Author: Jonas Wallin 

clearvars
close all

m = 70;
n = 81;

d = 2;

N = ceil(0.2*m*n);

mask = true(m,n);

car_order = 1;

K = 1;
theta = cell(1,K);

kp2 = 0.05;

theta{1}.Qd = [1 0.3;0.3 1];
theta{1}.kappa2 = kp2
theta{1}.mu = 2*1*(1:d)';
theta{1}.order = car_order;

sigma_e = ones(1,d)*sqrt(0.1);

[y, x, z] = lgfm_gendat(mask,[],[],theta,sigma_e);

if K>1  
  zplot = zeros(m*n,K); zplot(mask(:),:) = z;
end
xplot = zeros(m*n,size(x,2)); xplot(mask(:),:) = x;
%Xd = reshape(X(:,1),[size(C,1) d]);

%extract some observations
obs_ind = randperm(length(y)); obs_ind = obs_ind(1:N);
Y = y(obs_ind,:);

obs_plot = nan(size(y));
obs_plot(obs_ind,:) = Y;
figure(6); 

k = 1;
for i = 1:d
  subplot(2,d,k)
  imagesc(reshape(xplot(:,i),[m n 1])); colorbar;axis image
  subplot(2,d,k+1)
  h = imagesc(reshape(obs_plot(:,i),[m n]))
  set(h,'alphadata',~isnan(reshape(obs_plot(:,i),[m n])));axis image
  colorbar  
  k = k+2;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate Gaussian estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theta_start = struct('logkappa2', 2*log(sqrt(2*8)/(0.4*m)),...
                     'logsigma2', log(var(Y)/10)', 'Qd', diag(1./var(Y)), 'mu0', mean(Y)');

opts = struct('car_order', car_order,'tol',1e-6,'fixed_trace',0,'Ntrace',10,'step_rate',0.1,'step0',1);
[gtheta, Xk] = gaussian_ecg(Y,mask,obs_ind,theta_start,opts,500);

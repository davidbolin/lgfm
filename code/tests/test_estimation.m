clearvars
close all

m = 70;
n = 81;

K = 2;
d = 1;

N = ceil(0.2*m*n);

mask = true(m,n);

car_order = 1;
beta = 0.8*ones(1,K);
alpha = ones(1,K)/K;

K = 2;
theta = cell(1,K);
range = 40;
kp2 = 0.05;
for i=1:K
  theta{i}.Qd = i;
  theta{i}.kappa2 = kp2;
  theta{i}.mu = 4*i*(1:d)';
  theta{i}.order = car_order;
end
sigma_e = ones(1,d)*sqrt(0.001);


[y, x, z] = lgfm_gendat(mask,alpha,beta,theta,sigma_e);

zplot = zeros(m*n,K); zplot(mask(:),:) = z;

xplot = zeros(m*n,size(x,2)); xplot(mask(:),:) = x;
%extract some observations
obs_ind = randperm(length(y)); obs_ind = obs_ind(1:N);
Y = y(obs_ind,:);

obs_plot = nan(size(y));
obs_plot(obs_ind,:) = Y;

figure(6);
subplot(131)
imagesc(reshape(xplot,[m n 1])); colorbar;axis image
subplot(132)
imagesc(reshape(zplot(:,2),[m n]));axis image
subplot(133)
h = imagesc(reshape(obs_plot,[m n]));
set(h,'alphadata',~isnan(reshape(obs_plot(:,1),[m n])));axis image
colorbar

opts = struct('car_order', car_order,'tol',1e-6,'Ntrace',20,'step_rate',0.1,'step0',0.2,'plot',1);
[gauss_obj, z0] = lgfm_generate_startvalue(Y, mask,obs_ind,K,opts, 100);


opts = struct('gibbs_iter',5,'tol',0,'alpha_type',-1,'plot',2,...
          'Ntrace',10,'step_rate',0.01,'step0',0.2,'beta0',0.5);
[theta_lgrf, z_lgrf, p] = lgfm_ecg(Y,mask,obs_ind, z0, gauss_obj,opts,1000);

theta_lgrf.gauss_obj.Aplot = A;
theta_lgrf.gauss_obj.Bplot = ones(length(A),1);
[Xe Mz] = lgfm_kriging(z_lgrf{1},theta_lgrf,100,mask,obs_ind);


if estimate_gauss == 1
  theta_start = struct('logkappa2', 2*log(sqrt(2*8)/(0.4*m)),...
                     'logsigma2', log(var(Y)/10), 'Qd', 1/var(Y), 'mu0', mean(Y));

  opts = struct('car_order', 1,'tol',1e-6,'fixed_trace',0,'Ntrace',10,'step_rate',0.1,'step0',0.5);
  [gtheta, Xk] = gaussian_ecg(Y,mask,obs_ind,theta_start,opts,100);
end
%plot estimates
if 1
Gaussplot = zeros(m*n,size(x,2)); Gaussplot(mask(:),:) = A*gtheta.Xk{1};
figure(7)
subplot(321)
imagesc(reshape(Gaussplot(:,1),[m n]))
title('Gaussian est 1')
colorbar
subplot(322)
imagesc(reshape(Gaussplot(:,2),[m n]))
colorbar
title('Gaussian est 2')
subplot(323)
imagesc(reshape(Xe(:,1),[m n]))
colorbar
title('LMRF est 1')
subplot(324)
imagesc(reshape(Xe(:,2),[m n]))
colorbar
title('LMRF est 2')
subplot(325)
imagesc(reshape(x(:,1),[m n]))
colorbar
title('X1')
subplot(326)
imagesc(reshape(x(:,2),[m n]))
colorbar
title('X2')


figure(8)
subplot(221)
imagesc(reshape(x(:,1)-Gaussplot(:,1),[m n]))
title('Gaussian est 1')
colorbar
subplot(222)
imagesc(reshape(x(:,2)-Gaussplot(:,2),[m n]))
colorbar
title('Gaussian est 2')
subplot(223)
imagesc(reshape(x(:,1)-Xe(:,1),[m n]))
colorbar
title('LMRF est 1')
subplot(224)
imagesc(reshape(x(:,2)-Xe(:,2),[m n]))
colorbar
title('LMRF est 2')
end

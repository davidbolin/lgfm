function [EX, VX] = gaussian_krig(fnbr,g_obj,Y,mask,obs_ind)

%calculates gaussian mean and variance

%build CAR matrices:
G = stencil2prec(mask,[0 -1 0;-1 4 -1;0 -1 0]);
C = speye(numel(mask));
C = C(mask(:),mask(:));
A = C(obs_ind,:);

B = g_obj.B{fnbr};
Qd = g_obj.Qd;
n = size(Y,1);
dy = size(Y,2);
Y = Y(:);

kp2 = g_obj.kappa2;
sigma2 = g_obj.sigma2;
Rd = chol(Qd);

if g_obj.order == 2
  K = G+kp2*C;
  Qk = K'*K/(4*pi*kp2);
else %CAR1 uses cholesky
  %K = chol(g_obj.G+kp2*g_obj.C);
  Qk = G+kp2*C;
end
Q = kron(Qd,Qk);


Qsig = sparse(diag(1./sigma2));
Qh_p = Q + kron(Qsig,A'*A);
reo = amd(Qh_p);
ireo(reo) = 1:length(reo);
Qh_p = Qh_p(reo,reo);

%Calculate the mean
R = chol(Qh_p);
QA =kron(Qsig,A');
b = QA(reo,:)*(Y-B*g_obj.mu);
Bx = kron(eye(dy),ones(size(C,1),length(g_obj.mu)/dy));
Bx = Bx(reo,:);
EX = Bx*g_obj.mu + R\(R'\b);
VX = diag(Qinv(R));
EX = EX(ireo);
VX = VX(ireo);

EX = reshape(EX,[sum(mask(:)) dy]);
VX = reshape(VX,[sum(mask(:)) dy]);


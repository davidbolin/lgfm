function [parameters, Xk] = gaussian_ecg(Y,mask,obs_ind,par0,opts, iter)
%
% [parameters Xk] = gaussian_ecg3(Y,A,G,C,par0,opts, iter)
%	Function that estimates parameters in a possibly multivariate LGM
%   Y = A*(mu+X) + e
% where e is Gaussian measurement noise with variance sigma2 and X has a
% Matern SPDE prior:
%   (kappa2 - Delta)X(s) = W(s)
% If X is multivariate, the precision matrix is assumed to be on the form
% Q = Qs kron Qd where Qs is the precision matrix a CAR model and Qd
% determines the multivariate structure.
% INPUTS:
% Y       : Measured data. Can be a matrix or a cell. The cell structure is used
%          if one has replicated measurement from several fields X with the
%          same prior.
% mask    : an m-by-n matrix specifying the domain of interest.
% obs_ind : the indices where Y is observed.
% par0    : Starting values for the parameters:
%             logkappa2 : log(kappa2)
%             logsigma2 : log(sigma2)
%             mu0       : mu
%             Qd        : multivariate part.
% opts    : Structure with various options:
%           Qd_fixed = 1 : Assume that the matrix Qd determining the
%                          multivariate part is fixed (and given by par0.Qd).
%           common_sigma : Use a common measurement noise variance for each
%                          dimension.
%           sigma_fixed  : Use this value as a known value for the
%                          measurement noise variance.
%           plot         : Plot parameter tracks?
%           Ntrace       : Number of MC samples for trace estimation.
%           fixed_trace  : Use fixed random vectors for the trace est?
%                          default 0.
%           tol          : Tolerance for convergence.
%           step_rate    : scaling of step size iter^(-step_rate)
%           step0        : initial step size.
%           car_order    : the order of the CAR model.
%           learning_rate: learning rate for stochcastic gradients.
% iter : Max number of iterations in the estimation method.
%	OUTPUTS:
% parameters : Structure with estimates of the parameters.
% Xk      : Posterior means of the fields X.

use_hess = 1;

if nargin < 6; iter = 20; end
if nargin < 5; opts = []; end
if nargin < 4; par0 = []; end

if iscell(Y);
  nrep = length(Y);
  if ~iscell(mask) || ~iscell(obs_ind)
    error('mask and obs_ind should be cells if Y is.')
  end
else
  nrep = 1;
  Y  = {Y};
  mask = {mask};
  obs_ind = {obs_ind};
end

%build CAR matrices:
C = cell(1,nrep);
G = cell(1,nrep);
A = cell(1,nrep);
for i=1:nrep
  G{i} = stencil2prec(mask{i},[0 -1 0;-1 4 -1;0 -1 0]);
  C{i} = speye(numel(mask{i}));
  C{i} = C{i}(mask{i}(:),mask{i}(:));
  A{i} = C{i}(obs_ind{i},:);
end


d = size(Y{1},2);
nd = cell(1,nrep);
n = cell(1,nrep);
for rep = 1:nrep
  nd{rep} = size(Y{rep},1);
  n{rep}  = size(C{rep},1);
end

if isempty(opts)
  Qd_fixed = [];
  common_sigma = 0;
  sigma_fixed = [];
  do_plot = 1;
  N = 10;
  car_order = 2;
  tol = 1e-5;
  fixed_trace = 0;
  step_rate = 0.1;
  step0 = 1;
  learning_rate = 0.5;
else
  names = fieldnames(opts);
  if sum(strcmp(names,'car_order'))>0
    car_order = opts.car_order;
  else
    car_order = 2;
  end

  if sum(strcmp(names,'Qd_fixed'))>0
    Qd_fixed = opts.Qd_fixed;
  else
    Qd_fixed = [];
  end
  if sum(strcmp(names,'learning_rate'))>0
    learning_rate = opts.learning_rate;
  else
    learning_rate = 0.5;
  end
  if sum(strcmp(names,'common_sigma'))>0
    common_sigma = opts.common_sigma;
  else
    common_sigma = 0;
  end
  if sum(strcmp(names,'sigma_fixed'))>0
    sigma_fixed = opts.sigma_fixed;
    if size(sigma_fixed,1)<size(sigma_fixed,2)
      sigma_fixed = sigma_fixed';
    end
  else
    sigma_fixed = [];
  end
  if sum(strcmp(names,'plot'))>0
    do_plot = opts.plot;
  else
    do_plot = 1;
  end
  if sum(strcmp(names,'step0'))>0
    step0 = opts.step0;
  else
    step0 = 1;
  end
  if sum(strcmp(names,'Ntrace'))>0
    N = opts.Ntrace;
  else
    N = 10;
  end
  if sum(strcmp(names,'tol'))>0
    tol = opts.tol;
  else
    tol = 1e-5;
  end
  if sum(strcmp(names,'fixed_trace'))>0
    fixed_trace = opts.fixed_trace;
  else
    fixed_trace = 0;
  end
  if sum(strcmp(names,'step_rate'))>0
    step_rate = opts.step_rate;
  else
    step_rate = 0.1;
  end
end

if common_sigma;
  sigma_len = 1;
else
  sigma_len = d;
end
phi_len = 1; kp_len = 1;

tmp = ones(d,d); ttmp = triu(tmp);
trilind = find(ttmp);
diagind = find(eye(d));
diagonalind = cumsum(1:d);

tmp = 0;
AtA = cell(1,nrep);
reo_Q =cell(1,nrep);
reo = cell(1,nrep);
ireo = cell(1,nrep);
Yk = cell(1,nrep);
B = cell(1,nrep);
for rep = 1:nrep
  AtA{rep} = A{rep}'*A{rep};
  if car_order == 1
    Qk = (G{rep} + C{rep});
  else
    Qk = (G{rep} + C{rep})'*(G{rep} + C{rep})/(4*pi);
  end
  reo_Q{rep} = amd(Qk);
  reo{rep} = amd(kron(ttmp'*ttmp,Qk)+kron(eye(d),AtA{rep}));
  ireo{rep}(reo{rep}) = 1:length(reo{rep});
  tmp = tmp + (Y{rep}-repmat(mean(Y{rep}),[size(Y{rep},1) 1]))' ...
      *(Y{rep}-repmat(mean(Y{rep}),[size(Y{rep},1) 1]))/size(Y{rep},1);
  Yk{rep} = Y{rep};
  Y{rep} = Y{rep}(:);
  B{rep} = kron(eye(d),ones(nd{rep},1));
end

tmpadd = 0;
if min(eig(tmp)) < 0;
  tmpadd = -1.1*min(eig(tmp));
end

qtmp = chol(inv(tmp+tmpadd));
theta0 = qtmp(trilind);
theta0(diagonalind) = log(theta0(diagonalind));

if isempty(par0)
  kp0 = 1;
  sigma0 = zeros(sigma_len,1);
  mu0 = zeros(d,1);
else
  names = fieldnames(par0);
  if sum(strcmp(names,'logkappa2'))>0
    kp0 = par0.logkappa2;
  else
    kp0 = 1;
  end
  if sum(strcmp(names,'logsigma2'))>0
    sigma0 = par0.logsigma2;
  else
    sigma0 = zeros(sigma_len,1);
  end
  if sum(strcmp(names,'mu0'))>0
    mu0 = par0.mu0;
  else
    mu0 = zeros(d,1);
  end
  if sum(strcmp(names,'Qd'))>0
    Rd0 = chol(par0.Qd);
    theta0 = Rd0(trilind);
    theta0(diagonalind) = log(theta0(diagonalind));
  elseif sum(strcmp(names,'Sigma'))>0
    Rd0 = chol(inv(par0.Sigma));
    theta0 = Rd0(trilind);
    theta0(diagonalind) = log(theta0(diagonalind));
  end
end

if ~isempty(sigma_fixed);
  sigma_len = 0;
  sigma2 = sigma_fixed;
  logsigma2 = log(sigma_fixed);
  sigma0 = [];
end

if ~isempty(Qd_fixed)
  Qd = Qd_fixed;
  Rd = chol(Qd);
  theta0 = [];
end

P =[kp0;sigma0;theta0;mu0];

kp2 = exp(P(1:kp_len));
if isempty(sigma_fixed)
  sigma2 = exp(P(1+kp_len:kp_len+sigma_len));
end

if isempty(Qd_fixed)
  theta = P(1+kp_len+sigma_len:kp_len+sigma_len+d*(d+1)/2);
  Rd = zeros(d,d);
  Rd(trilind) = theta;
  Rd(diagind) = exp(Rd(diagind));
  Qd = Rd'*Rd;
end

beta_mu = P(kp_len+sigma_len+d*(d+1)/2+1:end);

dmuc          = cell(1,rep);
dsigmac       = cell(1,rep);
dkappac       = cell(1,rep);
d2muc         = cell(1,rep);
d2thetac      = cell(1,rep);
d2sigmac      = cell(1,rep);
dsigmadmuc    = cell(1,rep);
dthetadkappac = cell(1,rep);
dthetac       = cell(1,rep);

for rep = 1:nrep
  dmuc{rep}           = zeros(d,1);
  dsigmac{rep}        = zeros(d,1);
  dkappac{rep}        = 0;
  d2muc{rep}          = zeros(d,1);
  d2thetac{rep}       = zeros(d*(d+1)/2, d*(d+1)/2);
  d2sigmac{rep}       = zeros(d,1);
  dsigmadmuc{rep}     = zeros(d,1);
  dthetadkappac{rep}  = zeros(d*(d+1)/2,1);
  dthetac{rep}        = zeros(d*(d+1)/2,1);
end

dmu          = zeros(d,1);
dsigma       = zeros(d,1);
dkappa       = 0;
d2kappa      = 0;
d2mu         = zeros(d,1);
d2theta      = zeros(d*(d+1)/2, d*(d+1)/2);
d2sigma      = zeros(d,1);
dsigmadmu    = zeros(d,1);
dthetadkappa = zeros(d*(d+1)/2,1);
dtheta       = zeros(d*(d+1)/2,1);


Nabla = 0;
step = 0;
U = cell(nrep,1);
QU = cell(nrep,1);
Uk = cell(nrep,N);
QUk = cell(nrep,N);
X = cell(nrep,1);
Z = cell(nrep,1);

for rep = 1:nrep
  X{rep} = zeros(n{rep}*d,1);
  Z{rep} = zeros(n{rep}*d,1);
  U{rep} = zeros(n{rep}*d,N);
  QU{rep} = zeros(n{rep}*d,N);
  for ii=1:N
    U{rep}(:,ii) = 1-2*round(rand(n{rep}*d,1));
    Uk{rep,ii} = reshape(U{rep}(:,ii),[n{rep} d]);
  end
end

if do_plot
  hFig = figure();
end

latest_precon = 0;
gradient_old = 0;
tic;
stepsize_old = 1000;
for i=1:iter
  fprintf('%d : ',i)
  if do_plot
    kpsave(i) = kp2;
    musave(i,:) = beta_mu;
    sigmasave(i,:) = sigma2;
    thetasave(i,:) = Qd(trilind);
    set(0,'CurrentFigure',hFig);
    subplot(221); plot(kpsave); title('kappa2');
    subplot(222); plot(musave); title('mu');
    subplot(223); plot(sigmasave); title('sigma2');
    subplot(224); plot(thetasave); title('Qd');
    drawnow
  end
  dmuold = dmu;
  dsigmaold = dsigma;
  dkappaold = dkappa;
  dthetaold = dtheta;

  if common_sigma
    Qsig = speye(d)/sigma2;
  else
    Qsig = sparse(diag(1./sigma2));
  end
  for rep = 1:nrep
    if car_order == 1
      Qk = (G{rep}+kp2*C{rep});
    else
      Qk = (G{rep}+kp2*C{rep})'*(G{rep}+kp2*C{rep})/(4*pi*kp2);
    end
    Qh_p{rep} = kron(Qd,Qk) + kron(Qsig,AtA{rep});
    Qh_p{rep} = Qh_p{rep}(reo{rep},reo{rep});
  end

  %test if preconditioner needs to be updated
  update_precon = 0;
  if i == 1
    update_precon = 1;
  elseif abs(Nabla(d+1:end)'*step(d+1:end)) > 1
    update_precon = 1;
  elseif i - latest_precon > 10
    update_precon = 1;
  end

  if update_precon
    fprintf('update preconditioner, ')
    for rep = 1:nrep
      M1{rep} = RIC(Qh_p{rep},1e-3,0.6);
      M2{rep} = M1{rep}';
    end
    latest_precon = i;
  end

  fprintf('calculate mean, ')
  for rep = 1:nrep
    QA =kron(Qsig,A{rep}');
    b = QA(reo{rep},:)*(Y{rep}-B{rep}*beta_mu);
    [Z{rep} flag, relres] = pcg(Qh_p{rep},b,1e-3,500,M1{rep},M2{rep},Z{rep});
    if flag ~= 0
      disp(' ')
      disp('Warning pcg in the Mean estimation has not converged')
      disp(['Relative error = ', num2str(relres)])
      disp(['flag = ', num2str(flag)])
    end
    X{rep} = Z{rep}(ireo{rep});
    Xk{rep} = reshape(X{rep},[n{rep} d]);
  end

  fprintf('trace, ')
  for rep = 1:nrep
    i_trace_up = randperm(N); %update one of the random vectors for trace calc each iteration.
    for ii=1:N
      if ~fixed_trace && ii==i_trace_up(1)
        U{rep}(:,ii) = 1-2*round(rand(n{rep}*d,1));
        Uk{rep,ii} = reshape(U{rep}(:,ii),[n{rep} d]);
      end
      [QUi flag, relres] = pcg(Qh_p{rep},U{rep}(reo{rep},ii),1e-3,500,...
                               M1{rep},M2{rep},QU{rep}(reo{rep},ii));
      if flag ~= 0
        disp(' ')
        disp('Warning pcg in the Trace estimation has not converged')
        disp(['Relative error = ', num2str(relres)])
        disp(['flag = ', num2str(flag)])
      end
      QU{rep}(:,ii) = QUi(ireo{rep});
        QUk{rep,ii} = reshape(QU{rep}(:,ii),[n{rep} d]);
    end
  end
  fprintf('gradient. ')

  %\sigma and \mu %
  for rep = 1:nrep
    ba = [];
    beta_tmp = kron(Qsig,A{rep}'*ones(nd{rep},1));
    for k=1:d
      diffk = Yk{rep}(:,k)-A{rep}*Xk{rep}(:,k)-beta_mu(k);
      dmuc{rep}(k) = sum(diffk)/sigma2(k);
      tmp = sparse(d,d); tmp(k,k) = 1;
      trest = sum(diag(U{rep}'*kron(tmp,AtA{rep})*QU{rep}))/N;
      dsigmac{rep}(k)= -nd{rep}/2 + (diffk'*diffk + trest)/(2*sigma2(k));

      if use_hess
        d2sigmac{rep}(k) = -(diffk'*diffk+trest)/(2*sigma2(k));
        dsigmadmuc{rep}(k) = -dmuc{rep}(k);
        d2muc{rep}(k) = -nd{rep}/sigma2(k);
      end
    end
  end

  %\kappa and \theta %
  for rep = 1:nrep
    K = G{rep}+kp2*C{rep};
    if car_order == 1
      Qk = K;
      R_Q = chol(Qk(reo_Q{rep},reo_Q{rep}));
    else
      Qk = K'*K/(4*pi*kp2);
      R_Q = chol(4*pi*kp2*Qk(reo_Q{rep},reo_Q{rep}));
    end
    Qi = Qinv(R_Q);
    if car_order == 2
      KiC = Qi.*K(reo_Q{rep},reo_Q{rep});
      KiCKiC = Qi.*C{rep}(reo_Q{rep},reo_Q{rep});
      trKiC = sum(sum(KiC));
    else
      KiC = Qi;
      KiCKiC = KiC*KiC;
      trKiC = sum(diag(KiC));
      trKiCKiC = sum(diag(KiCKiC));
    end
    trestG = 0;
    trestC = 0;
    trestQ = 0;
    for ii=1:N
      trestG = trestG + sum(sum(Uk{rep,ii}.*(G{rep}*QUk{rep,ii}*Qd)));
      trestC = trestC + sum(sum(Uk{rep,ii}.*(C{rep}*QUk{rep,ii}*Qd)));
      trestQ = trestQ + sum(sum(Uk{rep,ii}.*(Qk*QUk{rep,ii}*Qd)));
    end
    trestG = trestG/N;
    trestC = trestC/N;
    trestQ = trestQ/N;

    if car_order == 1 %all car1 was without /2 (so *2 for hess)
      dkappac{rep} = (kp2*d*trKiC - kp2*sum(sum(Xk{rep}.*(C{rep}*Xk{rep}*Qd))) - kp2*trestC)/2;
      if use_hess
        d2kappac{rep}=  dkappac{rep} - d*kp2^2*trKiCKiC;
      end
    else
      dkappac{rep} = - d*n{rep}/2 + kp2*d*trKiC ...
                   - sum(sum(Xk{rep}.*(K*Xk{rep}*Qd)))/(4*pi) ...
                   + sum(sum(Xk{rep}.*(Qk*Xk{rep}*Qd)))/2 ...
                   - (trestG+kp2*trestC)/(4*pi) + trestQ/2;

      if use_hess
        d2kappac{rep}= kp2*d*trKiC -d*kp2^2*sum(sum(KiCKiC)) ...
                    - sum(sum(Xk{rep}.*(Qk*Xk{rep}*Qd)))/2 ...
                    + sum(sum(Xk{rep}.*(K*Xk{rep}*Qd)))/(4*pi) ...
                    - kp2*sum(sum(Xk{rep}.*(C{rep}*Xk{rep}*Qd)))/(4*pi) ...
                    - trestQ/2 - kp2*trestC/(4*pi) + (trestG+kp2*trestC)/(4*pi);
      end
    end
    %d\theta
    for k=1:d*(d+1)/2
      dkR = sparse(d,d); dkR(trilind(k)) = 1;
      dlogdet = 0;
      if sum(trilind(k)==diagind)>0
        dkR(trilind(k)) = exp(theta(k));
        dlogdet = 1;
      end
      dQd = sparse(dkR'*Rd + Rd'*dkR);
      trest = 0;
      trest2 = 0;
      for ii=1:N
        trest = trest + sum(sum(Uk{rep,ii}.*(Qk*QUk{rep,ii}*dQd)));
        if car_order == 2
          trest2 = trest2 + sum(sum(Uk{rep,ii}.*(K*QUk{rep,ii}*dQd)));
        else
          trest2 = trest2 + sum(sum(Uk{rep,ii}.*(C{rep}*QUk{rep,ii}*dQd)));
        end
      end
      trest = trest/N; trest2 = trest2/N;
      dthetac{rep}(k) = (n{rep}*dlogdet ...
                      - sum(sum(Xk{rep}.*(Qk*Xk{rep}*dQd)))/2 - trest/2);

      if use_hess
        if car_order ==1
          dthetadkappac{rep}(k)=-kp2*(sum(sum(Xk{rep}.*(C{rep}*Xk{rep}*dQd))) + trest2)/2;
        else
          dthetadkappac{rep}(k)=-sum(sum(Xk{rep}.*(K*Xk{rep}*dQd)))/(4*pi) ...
              + sum(sum(Xk{rep}.*(Qk*Xk{rep}*dQd)))/2 + trest/2 - trest2/(4*pi);
        end
        for l=k:d*(d+1)/2
          d2Qd = 0;
          dlR = sparse(d,d); dlR(trilind(l)) = 1;
          if sum(trilind(l)==diagind)>0
            dlR(trilind(l)) = exp(theta(l));
            if k==l; d2Qd = dQd; end
          end
          d2Qd = sparse(d2Qd + dkR'*dlR + dlR'*dkR);
          trest = 0;
          if sum(abs(d2Qd(:)))>0
            for ii=1:N
              trest =trest+ sum(sum(Uk{rep,ii}.*(Qk*QUk{rep,ii}*d2Qd)));
            end
            trest = trest/N;
          end
          d2thetac{rep}(k,l) = -sum(sum(Xk{rep}.*(Qk*Xk{rep}*d2Qd)))/2 -trest/2;
          d2thetac{rep}(l,k) = d2thetac{rep}(k,l);
        end
      end
    end
  end
  %%%%%%%%%%%%%%%%%%%%%
  % update parameters %
  %%%%%%%%%%%%%%%%%%%%%
  dmu=zeros(d,1);

  dkappa = 0;
  d2kappa = 0;
  d2mu = zeros(d,1);
  d2theta = zeros(d*(d+1)/2, d*(d+1)/2);
  dthetadkappa = zeros(d*(d+1)/2,1);
  dtheta = zeros(d*(d+1)/2,1);
  if sigma_len > 0
    dsigma = zeros(d,1);
    d2sigma = zeros(d,1);
    dsigmadmu = zeros(d,1);
  end
  for rep = 1:nrep
    dmu           = dmu           + dmuc{rep};
    if sigma_len > 0
      dsigma        = dsigma        + dsigmac{rep};
      d2sigma       = d2sigma       + d2sigmac{rep};
      dsigmadmu     = dsigmadmu     + dsigmadmuc{rep};
    end
    dkappa        = dkappa        + dkappac{rep};
    dtheta        = dtheta        + dthetac{rep};
    d2mu          = d2mu          + d2muc{rep};
    d2kappa       = d2kappa       + d2kappac{rep};
    d2theta       = d2theta       + d2thetac{rep};
    dthetadkappa  = dthetadkappa  + dthetadkappac{rep};
  end
  if sigma_len > 0
    H = [diag(d2mu)         diag(dsigmadmu)             zeros(d,1)           zeros(d,d*(d+1)/2);
         diag(dsigmadmu)    diag(d2sigma)               zeros(sigma_len,1)   zeros(sigma_len,d*(d+1)/2);
         zeros(1,d)         zeros(1,sigma_len)          d2kappa              dthetadkappa';
         zeros(d*(d+1)/2,d) zeros(d*(d+1)/2,sigma_len)  dthetadkappa         d2theta];
     Pold = [beta_mu;log(sigma2);log(kp2);theta];
     Nablaold = [dmuold;dsigmaold;dkappaold;dthetaold];
  Nabla = [dmu;dsigma;dkappa;dtheta];
  else
     H = [diag(d2mu)           zeros(d,1)       zeros(d,d*(d+1)/2);
         zeros(1,d)         d2kappa          dthetadkappa';
         zeros(d*(d+1)/2,d) dthetadkappa     d2theta];
     Pold = [beta_mu;log(kp2);theta];
     Nablaold = [dmuold;dkappaold;dthetaold];
     Nabla = [dmu;dkappa;dtheta];
  end

  step_old = step;

  if max(eig(H)) > 0
    dsigmadmu = zeros(size(dsigmadmu));
    dthetadkappa = zeros(size(dthetadkappa));
    H = [diag(d2mu)         diag(dsigmadmu)             zeros(d,1)           zeros(d,d*(d+1)/2);
         diag(dsigmadmu)    diag(d2sigma)               zeros(sigma_len,1)   zeros(sigma_len,d*(d+1)/2);
         zeros(1,d)         zeros(1,sigma_len)          d2kappa              dthetadkappa';
         zeros(d*(d+1)/2,d) zeros(d*(d+1)/2,sigma_len)  dthetadkappa         d2theta];
    if max(eig(H)) > 0
      H = -diag(abs(diag(H)));
      if max(eig(H)) > 0
        disp(' Warning diag(H) not neg def ')
        disp(' The optimzation is now unstable')
      end
    end
  end
  H1 = H(1:1+d*(d+1)/2,1:1+d*(d+1)/2);
  H2 = H(2+d*(d+1)/2:end,2+d*(d+1)/2:end);
  H1pre = diag(1./diag(H1));
  H2pre = diag(1./diag(H2));
  step1 = (H1pre*H1)\(H1pre*Nabla(1:1+d*(d+1)/2));
  step2 = (H2pre*H2)\(H2pre*Nabla(2+d*(d+1)/2:end));

  step = [step1;step2];
  gradient = learning_rate*gradient_old + step;
  gamma =step0*i^(-step_rate);
  P = Pold - gamma*gradient;
  gradient_old = gradient;

  beta_mu = P(1:d);
  if sigma_len>0
    logsigma2 = P(d+1:2*d);
    sigma2 = max(exp(logsigma2),1e-10);
  end
  logkp2 = P(d+1+sigma_len);
  if logkp2 < log(1e-16) %to avoid numerical instabilities
    logkp2 = log(1e-16);
  end
  theta = P(d+2+sigma_len:end);

  Rd = sparse(d,d);
  Rd(trilind) = theta;
  Rd(diagind) = exp(Rd(diagind));
  Qd = full(Rd'*Rd);
  kp2 = exp(logkp2);
  stepsize = abs(Nabla'*step);
  fprintf('stepsize %f\n',(stepsize + stepsize_old)/2)
  if (stepsize + stepsize_old)/2 < tol; break; end
  stepsize_old = stepsize;
end
esttime = toc;
fprintf('optimization time: %f\n',esttime)

parameters.P = P;
parameters.kappa2 = exp(logkp2);
parameters.sigma2 = exp(logsigma2);
parameters.Qd = Qd;
parameters.mu = beta_mu;
parameters.B = B;
parameters.order = car_order;

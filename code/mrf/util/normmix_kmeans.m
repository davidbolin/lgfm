function [theta0,prior0,p,ll]=normmix_kmeans(x,K,maxiter)
% Parse input parameters
if nargin<3, maxiter = []; end
if nargin<4, plotflag = []; end
if isempty(maxiter), maxiter = 1; end

% Use some steps of K-means for rough initial estimates:
[cl,theta0] = kmeans(x,K,maxiter);
% pi-estimates (use uniform pi):
prior0 = ones(1,K)/K;
% Sigma-estimates (use common, isotropic Sigma):
n = size(x,1);
d = size(x,2);
Sigma = 0;
for k=1:K
  y = x(cl==k,:)-repmat(theta0{k}.mu,[sum(cl==k),1]);
  Sigma = Sigma + sum(sum(y.*y,1),2);
end
Sigma = eye(d)*Sigma/n/d;
for k=1:K
  theta0{k}.Sigma = Sigma;
end

%calculate log-like
pp = zeros(n,1);

spd = 1;
for k=1:K
  if min(eig(theta0{k}.Sigma))>0
    pp = pp + (1/K)*mvnpdf(x,theta0{k}.mu,theta0{k}.Sigma);
  else
    spd = 0;
  end
end
if spd == 1
  ll = sum(log(pp));
else
  ll = -Inf;
end

%calculate probabilities
p = zeros(n,K);
for k=1:K
  y = bsxfun(@minus,x,theta0{k}.mu);
  p(:,k) = exp(-0.5*sum( ((y*inv(theta0{k}.Sigma)).*y) ,2) ) / ...
	   ((2*pi)^(d/2)*det(theta0{k}.Sigma)^0.5);
end
p = p*diag(ones(1,K)/K);
p = p./repmat(sum(p,2),[1,K]);








function [cl,theta]=kmeans(x,K,maxiter)

if nargin<3, maxiter = []; end
if nargin<4, plotflag = []; end
if isempty(maxiter), maxiter = inf; end

[n,d] = size(x);

% Find unique starting points:
if (n<K), error('Not enough data!'); end
start_idx = ceil(rand(K,1)*n);
mu = x(start_idx,:);
while (size(unique(mu,'rows'),1)<K)
  start_idx = ceil(rand(K,1)*n);
  mu = x(start_idx,:);
end

cl_old = ones(n,1);
cl = zeros(n,1);

loop = 0;
mu_history(:,:,loop+1) = mu;
while (~all(cl==cl_old)) & (loop<maxiter)
  loop = loop+1;
  cl_old = cl;
  % Squared distances:
  for i=1:K
    %dist2(:,i) = sum((x-ones(n,1)*mu(i,:)).^2,2);
    dist2(:,i) = sum((bsxfun(@minus,x,mu(i,:))).^2,2);
  end
  % Classify:
  [tmp,cl] = min(dist2,[],2);
  % New mu estimates
  for i=1:K
    mu(i,:) = mean(x(cl==i,:),1);
  end
end

% Collect output:
for k=1:K
  theta{k}.mu = mu(k,:);
end

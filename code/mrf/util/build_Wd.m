function W = build_Wd(N,mask)
%N neighborhoodstructure

%mask either a mask of size mxn (2d) or mxnxl (3d) or a vector containing the size of the image [m n] (2d) or [m n l] (3d)

if min(size(mask))==1
	sz = mask;
	usemask = 0;
else
	sz = size(mask);
	usemask = 1;
end
m = sz(end); n = sz(end-1);
W1=spdiags([ones(m,1) zeros(m,1) ones(m,1)],[-1:1],m,m);
W2=spdiags([ones(n,1) zeros(n,1) ones(n,1)],[-1:1],n,n);
W1(1,2) = 0; W1(end,end-1)=0; W2(1,2) = 0; W2(end,end-1)=0;
W1c = sparse(m,m); W1c(1,1:2) = 1;  W1c(end,end-1:end)=1;
W2c = sparse(n,n); W2c(1,1:2) = 1;  W2c(end,end-1:end)=1;

E1 = speye(m);
E2 = speye(n);
if length(size(N))==2
	W = kron(W1,E2) + kron(E1,W2);
	W = W + kron(W1c,W2c); %add corner nodes
elseif length(size(N))==3
	l = sz(1);
	W3=spdiags([ones(l,1) zeros(l,1) ones(l,1)],[-1:1],l,l);
	W3(1,2) = 0; W3(end,end-1)=0;
	W3c = sparse(l,l); W3c(1,1:2) = 1;  W3c(end,end-1:end)=1;
	E3 = speye(l);
	W = kron(kron(W1,E2),E3) + kron(kron(E1,W2),E3) + kron(kron(E1,E2),W3);
	W = W + kron(kron(W1c,W2c),W3c); %add corner nodes
else
	error('only 2d and 3d implemented')
end

if usemask
	W = W(mask(:),mask(:));
end
  
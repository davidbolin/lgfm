
function [cl,theta]=kmeans(x,K,maxiter)

if nargin<3, maxiter = []; end
if nargin<4, plotflag = []; end
if isempty(maxiter), maxiter = inf; end

[n,d] = size(x);

% Find unique starting points:
if (n<K), error('Not enough data!'); end
start_idx = ceil(rand(K,1)*n);
mu = x(start_idx,:);
while (size(unique(mu,'rows'),1)<K)
  start_idx = ceil(rand(K,1)*n);
  mu = x(start_idx,:);
end

cl_old = ones(n,1);
cl = zeros(n,1);

loop = 0;
%reverseStr = '';
while (~all(cl==cl_old)) & (loop<maxiter)
  loop = loop+1;
  cl_old = cl;
%  msg = sprintf('iteration: %d %s %d %s', loop,' (max ', maxiter,')');
%  fprintf([reverseStr, msg]);
%  reverseStr = repmat(sprintf('\b'), 1, length(msg));
  % Squared distances:
  for i=1:K
    dist2(:,i) = sum((bsxfun(@minus,x,mu(i,:))).^2,2);
  end
  % Classify:
  [tmp,cl] = min(dist2,[],2);
  % New mu estimates
  for i=1:K
    mu(i,:) = mean(x(cl==i,:),1);
  end
end

% Collect output:
for k=1:K
  theta{k}.mu = mu(k,:);
end
%fprintf('\n')
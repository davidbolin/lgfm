function W = build_W(N,mask)

sz = size(mask);

if sum(N-rot90(N,2))
  error('The neighbourhood must have reflective symmetry.')
end
[a,b] = size(N);
if ((mod(a,2)~=1) || (mod(b,2)~=1))
  error('The neighbourhood must have odd width and height.')
end
a = ceil(a/2);
b = ceil(b/2);
if N(a,b)~=0
  error('The pixel cannot be neighbor with itself.')
end

II = [];
KK = [];
JJ_I = [];
JJ_J = [];

[I,J] = ndgrid(1:sz(1),1:sz(2));
I = I(:); J = J(:);
for i=1:size(N,1)
  for j=1:size(N,2)
    if (N(i,j) ~= 0)
      II = [II;I+sz(1)*(J-1)];
      JJ_I = [JJ_I;I+i-(size(N,1)+1)/2];
      JJ_J = [JJ_J;J+j-(size(N,2)+1)/2];
      KK = [KK; N(i,j)*ones(prod(sz),1)];
    end
  end
end
JJ = JJ_I+sz(1)*(JJ_J-1);
ok = (JJ_I>=1) & (JJ_I<=sz(1)) & (JJ_J>=1) & (JJ_J<=sz(2));
II(~ok) = []; JJ(~ok) = []; KK(~ok) = [];
W = sparse(II,JJ,KK,prod(sz),prod(sz));

W = W(mask(:)==1,mask(:)==1);
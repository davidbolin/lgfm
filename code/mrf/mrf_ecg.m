function [theta,z,p,converged,alpha,beta,timings]=mrf_ecg(y,K,mask,obs_ind,opts,iter)


% [theta,z,p,converged,alpha,beta,timings]=mrf_ecg(y,K,mask,obs_ind,opts,iter)
%	Function that estimates parameters in MRF mixture models with first order
%	neighborhoods on regular lattices in two or three dimensions.
%
%	INPUTS:
%	y		: data of size [N d] where N is the total number of measurements
%			  and d is the dimension. y can also be a cell with several images.
%	K		: The number of classes.
%	mask	: A mask of size [m n l] or [m n] that determines where the
%			  measurements are taken in the image.
% obs_ind : indices for the pixels that have observations.
% opts: structure with options
%       common_beta : 0/1 determines if a common beta should be used.
%       alpha_type  : 0/1 determines if a common alpha should be used.
%       sigma_type  : 0/1/2 determines the structure of the covariance
%                     matrix for the Gaussian components:
%            					 0 : no restriction on the covariance matrices
%                      1 : assume diagonal covariance matrices
%             				 2 : assume Sigma_k = sigma_k*eye(d)
%       common_sigma : 0/1 determines if a common covariance matrix should be
%                   	 used for all Gaussian components (with structure
%               			 determined by options(3)).
%        plot_flag : determines what should be plotted:
%                     0 : no plots
%                 		>0: plot classification
%                  		>1: also plot some parameter estimates.
%                 		>2: also plot posterior probability differences.
%         alpha0,beta0,p0,theta0 : starting values for parameters.
%         tol          : Tolerance for convergence.
%	OUTPUTS:
%	theta	: cell containing estimates of mean and covariance matrix for
%			  the mixture components.
%	z		: posterior estimate of the indicator field.
%	p		: posterior probabilities for each class.
%	converged:true if the estimator has converged, false otherwise
%	alpha	: estimates of the alpha parameters.
%	beta	: estimates of the beta parameters.

%%%%%%%%%%%%
if nargin < 6; iter = []; end
if nargin < 5; opts = []; end


if isempty(opts)
  alpha_type = 1;
  common_beta = 1;
  sigma_type = 0;
  common_sigma = 0;
  plotflag = 0;
  tol = 1e-5;
  gibbs_iter = 10;
  beta0 = [];
  alpha0 = [];
  p0 = [];
  theta0 = [];
else
  names = fieldnames(opts);
  if sum(strcmp(names,'alpha_type'))>0
    alpha_type = opts.alpha_type;
  else
    alpha_type = 1;
  end
  if sum(strcmp(names,'plot'))>0
    plotflag = opts.plot;
  else
    plotflag = 1;
  end
  if sum(strcmp(names,'sigma_type'))>0
    sigma_type = opts.sigma_type;
  else
    sigma_type = 0;
  end
  if sum(strcmp(names,'common_beta'))>0
    common_beta = opts.common_beta;
  else
    common_beta = 0;
  end
  if sum(strcmp(names,'common_sigma'))>0
    common_sigma = opts.common_sigma;
  else
    common_sigma = 0;
  end
  if sum(strcmp(names,'beta0'))>0
    beta0 = opts.beta0;
  else
    beta0 = [];
  end
  if sum(strcmp(names,'alpha0'))>0
    alpha0 = opts.alpha0;
  else
    alpha0 = [];
  end
  if sum(strcmp(names,'beta0'))>0
    beta0 = opts.beta0;
  else
    beta0 = [];
  end
  if sum(strcmp(names,'p0'))>0
    p0 = opts.p0;
  else
    p0 = [];
  end
  if sum(strcmp(names,'theta0'))>0
    theta0 = opts.theta0;
  else
    theta0 = [];
  end
  if sum(strcmp(names,'gibbs_iter'))>0
    gibbs_iter = opts.gibbs_iter;
  else
    gibbs_iter = 10;
  end
  if sum(strcmp(names,'tol'))>0
    tol = opts.tol;
  else
    tol = 1e-5;
  end
end

if isempty(iter), iter = 50; end
opt = [alpha_type common_beta sigma_type common_sigma];
sigma_opt = opt(3:4);
tic;

if iscell(y);
	nrep = length(y);
else
	nrep = 1;
	y  = {y};
	mask = {mask};
  obs_ind = {obs_ind};
	if length(y) ~= length(mask)
		error('A mask must be supplied for each image.')
	end
end

ntot = 0;
for rep = 1:nrep
	[m{rep} n{rep} l{rep}] = size(mask{rep});
  mn{rep} = m{rep}*n{rep};
	sz{rep} = size(y{rep});
	ntot = ntot + mn{rep};
	d{rep} = sz{rep}(end);
	if length(sz{rep}) == 3; y{rep} = y{rep}(:); end

	if length(size(mask{rep}))==3; threeD = 1; else; threeD = 0; end

	if threeD
		N = zeros(3,3,3);
		N(:,:,2) = [0 1 0; 1 0 1; 0 1 0];
		N(2,2,[1 3]) = 1;
 		if plotflag
	 		[XX{rep} YY{rep} ZZ{rep}] = meshgrid(1:m{rep},1:n{rep},1:l{rep});
			zmid{rep} = round(l{rep}/2);
			ymid{rep} = round(n{rep}/2);
			xmid{rep} = round(m{rep}/2);
		end
	else
		N = [0 1 0; 1 0 1; 0 1 0];
		if islogical(mask{rep})==0; mask{rep} = logical(mask{rep}); end
		if issparse(mask{rep})==0; mask{rep} = sparse(mask{rep}); end
	end

	mc{rep} = mask{rep}(:);

	if threeD
		W{rep} = build_Wd(N,mask{rep});
	else
		W{rep} = build_W(N,mask{rep});
	end
end

%obtain initial estimates of theta and z
ystacked = [];
for rep = 1:nrep; ystacked = [ystacked; y{rep}]; end
if isempty(alpha0) || isempty(beta0) || isempty(p0) || isempty(theta0)

  disp('Compute initial classification.')

  [theta,prior, pstacked,ll] = normmix_em(ystacked,K,2,0);
  %[theta,prior, pstacked,ll] = normmix_kmeans(ystacked,K,50);
  for i=1:10*K %repeat and choose classification with highest likelihood.
    [theta_new,prior_new, pstack_new,ll_new] = normmix_em(ystacked,K,1,0);
    if ll_new>ll
      theta = theta_new;
      pstacked = pstack_new;
      prior = prior_new;
      ll = ll_new;
    end
  end
  disp(ll)
  alpha=log(prior); beta = 0.5*ones(1,K);
else
	alpha = alpha0; beta = beta0;
	pstacked = p0; theta = theta0;
end
if opt(1) == 0
	alpha = log(ones(1,K)/K);
end
[tmp,clstacked]=max(pstacked,[],2);
if plotflag;
  pstack = zeros(prod(cell2mat(mn)),K);
end
for rep = 1:nrep
  p{rep} = zeros(mn{rep},K);
  cl{rep} = zeros(mn{rep},1);

  ps = pstacked(1:sz{rep}(1),:);
	cs = clstacked(1:sz{rep}(1));

	z0 = zeros(sz{rep}(1),K);
	for i=1:sz{rep}(1); z0(i,clstacked(i))=1; end
  z{rep} = zeros(mn{rep},K);
  for i=1:sz{rep}(1)
    z{rep}(obs_ind{rep}(i),:) = z0(i,:);
    p{rep}(obs_ind{rep}(i),:) = ps(i,:);
    cl{rep}(obs_ind{rep}(i)) = cs(i,:);
  end
  pstacked = pstacked(sz{rep}(1)+1:end,:);
  clstacked = clstacked(sz{rep}(1)+1:end);
  %draw random starting values for z at unobserved pixels
  for i=1:mn{rep}
    if sum(z{rep}(i,:))==0; z{rep}(i,randi(K,1)) = 1; end
  end

end
clear ystacked pstacked clstacked


%% plot %%
if plotflag
	figure(1),clf;
	if plotflag>1; figure(2);clf; end
	if plotflag>2; figure(3);clf; end
  	alpha_history(1,:) = exp(alpha);
    beta_history(1,:) = beta;
    for k=1:K
    	%beta_history(k,:,1) = beta;
  		mu_history(k,:,1) = theta{k}.mu;
  		if sigma_opt(1) == 0
	  		Sigma_history(:,:,k,1) = theta{k}.Sigma;
		elseif sigma_opt(1) == 1
			Sigma_history(:,k,1) = diag(theta{k}.Sigma);
		else
			Sigma_history(k,1) = theta{k}.Sigma(1);
		end
	end
	figure(1)
	for rep = 1:nrep
		if nrep < 6
			subplot(1,nrep,rep)
		else
			subplot(floor(sqrt(nrep)), ceil(nrep/floor(sqrt(nrep))), rep)
		end
		if threeD
			clc = zeros(m{rep}*n{rep}*l{rep},1);
			clc(mc{rep}) = cl{rep};
			clplot = reshape(clc,[m{rep} n{rep} l{rep}]);
			surf(squeeze(XX{rep}(:,:,zmid{rep})),...
				 squeeze(YY{rep}(:,:,zmid{rep})),...
				 squeeze(ZZ{rep}(:,:,zmid{rep})),...
				 squeeze(clplot(:,:,zmid{rep})),'EdgeColor','none')
			hold on
			surf(squeeze(XX{rep}(:,ymid{rep},:)),...
				 squeeze(YY{rep}(:,ymid{rep},:)),...
				 squeeze(ZZ{rep}(:,ymid{rep},:)),...
				 squeeze(clplot(:,ymid{rep},:)),'EdgeColor','none')
			surf(squeeze(XX{rep}(xmid{rep},:,:)),...
				 squeeze(YY{rep}(xmid{rep},:,:)),...
				 squeeze(ZZ{rep}(xmid{rep},:,:)),...
				 squeeze(clplot(xmid{rep},:,:)),'EdgeColor','none')
			axis image
		else
    		clplot = zeros(m{rep}*n{rep},1);
    		clplot(mc{rep}) = cl{rep};
    		imagesc(reshape(clplot,[m{rep} n{rep}])); axis off
    	end
    	drawnow
	end
end

loop = 0;
done = 0;

reverseStr = '';
grad_gauss = struct;
grad_gauss.alpha = 1;
grad_gauss.grad_prev = cell(1,K);
grad_gauss.d_prev    = cell(1,K);
for j=1:K
	grad_gauss.grad_prev{j} = [];
	grad_gauss.d_prev{j} = [];
end
grad_gauss.stepsize = 1;

grad_mrf = struct;
grad_mrf.alpha = 1;
grad_mrf.grad_prev = [];
grad_mrf.d_prev    = [];
grad_mrf.stepsize = 1;
grad_mrf.opt = opt(1);
grad_mrf.n = ntot;
stepsize = 1;
timings = toc;
while (~done)
	tic;
	loop = loop+1;
	if plotflag; p_old = pstack; end

	msg = sprintf('iteration: %d %s %d %s %d %s', loop,' (max ', iter,', stepsize', stepsize,')');
	fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
  	%disp('alpha post')
	alpha_post=mrf_gaussian_post(alpha,theta,y,sigma_opt,obs_ind,mask);

    if iscell(alpha_post) == 0
        alpha_post = {alpha_post};
    end
    for rep = 1:nrep
        alpha_post{rep} = alpha_post{rep} - max(alpha_post{rep},[],2)*ones(1,K);
    end

	update_mrf = 1;
	if loop>10
		if grad_mrf.stepsize < tol
			update_mrf = 0;
		end
		if mod(loop,10)==0
			update_mrf = 1;
		end
	end

	if update_mrf
    [z p da db H]=mrf_sim_grad(z,N,alpha_post,beta,gibbs_iter,mask,W,alpha,opt);
    [alpha beta grad_mrf] = mrf_take_step(alpha,beta,da,db,H,grad_mrf);
  end
  if loop<10
      theta=mrf_gaussian_est(p,y,sigma_opt,obs_ind);
  else
	  [theta, grad_gauss]=mrf_gaussian_grad2(p,y,theta, grad_gauss,obs_ind);
	end

	%if loop>5; grad_gauss.alpha=1; end

  	% The stopping criterion:
  	%[grad_mrf.stepsize grad_gauss.stepsize]
  	stepsize = abs(grad_mrf.stepsize + grad_gauss.stepsize);
  	converged = (loop>1) & (stepsize<tol);
  	done = converged | (loop>=iter);
	timings = [timings;toc];
  	%% plot %%
  	if plotflag
  		pstack = []; for rep = 1:nrep; pstack = [pstack;p{rep}]; end
 		p_diff(loop,1) = max(abs((p_old(:)-pstack(:))));

  		alpha_history(loop+1,:) = exp(alpha);
  		beta_history(loop+1,:) = beta;
    	for k=1:K
    		%beta_history(k,:,loop+1) = beta;
      		mu_history(k,:,loop+1) = theta{k}.mu;
      		if sigma_opt(1) == 0
	  			Sigma_history(:,:,k,loop+1) = theta{k}.Sigma;
			elseif sigma_opt(1) == 1
				Sigma_history(:,k,loop+1) = diag(theta{k}.Sigma);
			else
				Sigma_history(k,loop+1) = theta{k}.Sigma(1);
			end
    	end
    	set(0,'CurrentFigure',1)
    	for rep = 1:nrep
    		if nrep < 6
				subplot(1,nrep,rep)
			else
				subplot(floor(sqrt(nrep)), ceil(nrep/floor(sqrt(nrep))), rep)
			end
	    	[tmp,cl] = max(p{rep},[],2);
    		if threeD
				clc = zeros(m{rep}*n{rep}*l{rep},1);
				clc(mc{rep}) = cl;
				clplot = reshape(clc,[m{rep} n{rep} l{rep}]);
				surf(squeeze(XX{rep}(:,:,zmid{rep})),...
					 squeeze(YY{rep}(:,:,zmid{rep})),...
					 squeeze(ZZ{rep}(:,:,zmid{rep})),...
					 squeeze(clplot(:,:,zmid{rep})),'EdgeColor','none')
				hold on
				surf(squeeze(XX{rep}(:,ymid{rep},:)),...
					 squeeze(YY{rep}(:,ymid{rep},:)),...
					 squeeze(ZZ{rep}(:,ymid{rep},:)),...
					 squeeze(clplot(:,ymid{rep},:)),'EdgeColor','none')
				surf(squeeze(XX{rep}(xmid{rep},:,:)),...
					 squeeze(YY{rep}(xmid{rep},:,:)),...
					 squeeze(ZZ{rep}(xmid{rep},:,:)),...
					 squeeze(clplot(xmid{rep},:,:)),'EdgeColor','none')
				axis image; view(180,90)
			else
    			clplot = zeros(m{rep}*n{rep},K); clplot(mc{rep},:) = p{rep};
    			imagesc(rgbimage(reshape(clplot,[m{rep} n{rep} K]),jet(K)))
    			axis image; axis off
    		end
    	end
    	if plotflag>1;
    		set(0,'CurrentFigure',2)
    		subplot(221); plot(alpha_history); title('alpha')
    		subplot(222); plot(squeeze(beta_history)); title('beta')
    		subplot(223); plot(squeeze(mu_history(:,1,:))'); title('\mu_1')
    		subplot(224);
    		if sigma_opt(1) == 0
	  			plot(squeeze(Sigma_history(1,1,:,:))');title('\Sigma_{11}')
			elseif sigma_opt(1) == 1
				plot(squeeze(Sigma_history(1,:,:))');title('\Sigma_{11}')
			else
				plot(squeeze(Sigma_history(:,:))');title('\Sigma_{11}')
			end
    	end
    	if plotflag>2;
    		set(0,'CurrentFigure',3)
    		subplot(211);
    		lim = p_diff(loop);
          	p_diff_n = histc(p_old(:)-pstack(:),...
          					 linspace(-lim,lim,100))*100/length(pstack(:));
          	bar(linspace(-lim,lim,100),p_diff_n.^0.25,'histc');
          	axis([-lim,lim,0,max(p_diff_n).^0.25])
    		subplot(212); plot(log10(p_diff(:,2)));
    	end
    	title(loop)
    	drawnow
	end
end
fprintf('\n');
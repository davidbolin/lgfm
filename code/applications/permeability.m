
clearvars
close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Load the data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%The data used in this application can be downloaded from
%http://www.spe.org/web/csp/index.html

load spe_perm.dat
load spe_phi.dat
por = reshape(spe_phi',[60 220 85]);
perm = reshape(spe_perm',[60 220 85 3]);

Yperm = squeeze(log(perm(:,1:100,60,1))); %horizontal permeabillty
Ypor = squeeze(por(:,1:100,60)); %porosity

Y = Yperm;

%Plot the data
figure(1)
subplot(221)
imagesc(Yperm);axis image;colorbar;axis off
subplot(224)
imagesc(Ypor);axis image;colorbar;axis off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Extract data by taking every kk number of pixels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%corsening number:
kk = 2;

m = size(Y,1);
n = size(Y,2);

oi = zeros(m,n);
for i = 1:kk:m
  for j=1:kk:n
    oi(i,j) = 1;
  end
end
obs_ind = oi==1;
pred_ind = oi==0;
obs_ind = find(obs_ind(:));
pred_ind = find(pred_ind(:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot the data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mask = true(m,n);
Ye = Y(obs_ind);
Yp = Y(pred_ind);
obs_plot = nan(60*100,2);
obs_plot(obs_ind,:) = [Yperm(obs_ind) Ypor(obs_ind)];

subplot(222)
h = imagesc(reshape(obs_plot(:,1),[m n]));
set(h,'alphadata',~isnan(reshape(obs_plot(:,1),[m n])));axis image; axis off
colorbar
subplot(224)
h = imagesc(reshape(obs_plot(:,2),[m n]));
set(h,'alphadata',~isnan(reshape(obs_plot(:,1),[m n])));axis image; axis off
colorbar


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate MRF estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K_max = 10;
theta = cell(1,K_max);
z = cell(1,K_max);
p = cell(1,K_max);
alpha = cell(1,K_max);
beta = cell(1,K_max);
mse_MRF = zeros(K_max,1);
mae_MRF = zeros(K_max,1);
qign_MRF = zeros(K_max,1);
opts_mrf = struct('gibbs_iter',10,'tol',1e-8,'alpha_type',0,'plot',2);
for K=8:10
[theta{K},z{K},p{K},converged,alpha{K},beta{K}]=mrf_ecg(Ye,K,mask,obs_ind,opts_mrf,200);
  [krigMRF, varMRF] = mrf_kriging(Ye,z{K}{1},theta{K},alpha{K},beta{K},5000,mask,obs_ind);
  mse_mrf(K) = mean((Yp-krigMRF(pred_ind)).^2);
  mae_mrf(K) = mean(abs(Yp-krigMRF(pred_ind)));
  qign_mrf(K) = mean(((Yp-krigMRF(pred_ind))./(2*sqrt(varMRF(pred_ind)))).^2+log(sqrt(varMRF(pred_ind))));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate Gaussian estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta_lgfm = cell(1,K_max);
z_lgfm = cell(1,K_max);
p_lgfm = cell(1,K_max);
krigLGFM = cell(1,K_max);
varLGFM = cell(1,K_max);
mse_LGFM = zeros(K_max,1);
mae_LGFM = zeros(K_max,1);
qign_LGFM = zeros(K_max,1);

car_order = 1;
theta_start = struct('logkappa2', log(0.001),...
                     'logsigma2', log(var(Ye)/100)', 'Qd', diag(1./var(Ye)), 'mu0', mean(Ye)');

opts = struct('car_order', car_order,'tol',1e-8,'Ntrace',50,...
              'step_rate',0.1,'step0',0.5,'learning_rate',0.5,'plot',1);
gtheta = gaussian_ecg(Ye,mask,obs_ind,theta_start,opts,1000);
[krigGauss, varGauss] = gaussian_krig(1,gtheta,Ye,mask,obs_ind);
mse_LGFM(1,:) = mean((Yp-krigGauss(pred_ind,:)).^2);
mae_LGFM(1,:) = mean(abs(Yp-krigGauss(pred_ind,:)));
qign_LGFM(1,:) = mean(((Yp-krigGauss(pred_ind,:))./(2*sqrt(varGauss(pred_ind,:)+gtheta.sigma2))).^2+log(sqrt(varGauss(pred_ind,:)+gtheta.sigma2)));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate LGFM estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opts_init = struct('car_order', car_order,'tol',1e-5,'Ntrace',50,...
                    'step_rate',0.1,'step0',0.5,'plot',1,'learning_rate',0.2);
opts = struct('gibbs_iter',4,'tol',1e-4,'alpha_type',-1,'plot',2,...
          'Ntrace',20,'step_rate',0.01,'step0',0.5,'learning_rate',0.4);

for K=8:10
  close all
  %calculate starting values based on the MRF estimates
  tic
  [gauss_obj, z0] = lgfm_startvalue_from_mrf(Ye,p{K},theta{K},mask,obs_ind,opts_init,200);
  opts.beta0 = beta{K};
  opts.alpha0 = alpha{K};
  %estimate parameters
  [theta_lgfm{K}, z_lgfm{K}, p_lgfm{K}] = lgfm_ecg(Ye,mask,obs_ind, z0, gauss_obj,opts,1000);
  tLGFM(K,1) = toc;
  tic
  %compute kriging prediction
  [krigLGFM{K}, varLGFM{K}] = lgfm_kriging(z_lgfm{K}{1},theta_lgfm{K},1000,mask,obs_ind);
  tLGFM(K,2) = toc;
  mse_LGFM(K,:) = mean((Yp-krigLGFM{K}(pred_ind,:)).^2);
  mae_LGFM(K,:) = mean(abs(Yp-krigLGFM{K}(pred_ind,:)));
  qign_LGFM(K,:) = mean(((Yp-krigLGFM{K}(pred_ind,:))./(2*sqrt(varLGFM{K}(pred_ind,:)+theta_lgfm{K}.obj{1}.sigma2))).^2+log(sqrt(varLGFM{K}(pred_ind,:)+theta_lgfm{K}.obj{1}.sigma2)));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(2)
subplot(131)
plot(2:K_max,sqrt(mse_mrf(2:end)),'LineWidth',2)
hold on
plot(1:K_max,sqrt(mse_LGFM),'LineWidth',2)
ax = gca;
ax.XTick = 1:K_max;
legend('MRF models','LGMF models')
xlabel('K')
ylabel('RMSE')

subplot(132)
plot(2:K_max,mae_mrf(2:end),'LineWidth',2)
hold on
plot(1:K_max,mae_LGFM,'LineWidth',2)
ax = gca;
ax.XTick = 1:K_max;
legend('MRF models','LGMF models')
xlabel('K')
ylabel('MAE')

subplot(133)
plot(2:K_max,qign_mrf(2:K_max),'LineWidth',2)
hold on
plot(1:K_max,qign_LGFM,'LineWidth',2)
ax = gca;
ax.XTick = 1:K_max;
legend('MRF models','LGMF models')
xlabel('K')
ylabel('QIGN')


imagesc(Yperm)
c = caxis;
figure(3)
subplot(331)
imagesc(reshape(krigGauss,[m n]));
colorbar();axis image;axis off;colormap('default')
caxis(c)
subplot(334)
imagesc(reshape(krigLGFM{7},[m n]));
colorbar();axis image;axis off;colormap('default')
caxis(c)
subplot(337)
imagesc(reshape(krigMRF,[m n]));
colorbar();axis image;axis off;colormap('default')
caxis(c)

c = full([min([min(sqrt(varGauss)),min(sqrt(varLGFM{7})),min(sqrt(varMRF))]),...
max([max(sqrt(varGauss)),max(sqrt(varLGFM{7})),max(sqrt(varMRF))])]);
subplot(332)
imagesc(reshape(sqrt(varGauss),[m n]));
colrobar();axis image;axis off;colormap('copper')
caxis(c)

subplot(335)
imagesc(reshape(sqrt(varLGFM{7}),[m n]));
colrobar();axis image;axis off;colormap('copper')
caxis(c)
subplot(338)
imagesc(reshape(sqrt(varMRF),[m n]));
colrobar();axis image;axis off;colormap('copper')
caxis(c)

d1  =abs(krigGauss-Y(:));
d2 = abs(krigLGFM{7}-Y(:));
d3 = abs(krigMRF-Y(:));
c = full([min([min(d1),min(d2),min(d3)]),max([max(d1),max(d2),max(d3)])])
subplot(333)
imagesc(reshape(d1,[m n]));
colorbar();axis image;axis off;colormap('copper')
caxis(c)
subplot(336)
imagesc(reshape(d2,[m n]));
colorbar();axis image;axis off;colormap('copper')
caxis(c)
subplot(339)
imagesc(reshape(d3,[m n]));
colorbar();axis image;axis off;colormap('copper')
caxis(c)

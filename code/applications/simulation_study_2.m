clearvars
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Simulate data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m = 60;
n = 100;
d = 2;
mask = true(m,n);
car_order = 1;

theta = cell(1,1);
theta{1}.Qd = [2 1;1 2];
theta{1}.kappa2 = (0.1)^2;
theta{1}.mu = 0*(1:d)';
theta{1}.order = car_order;

sigma_e = ones(1,d)*sqrt(0.05);

%generate mean-zero field
[y, x] = lgfm_gendat(mask,[],[],theta,sigma_e);

%add mean value
mv = zeros(m,n);
mv(:,33:66) = 2;
mv(:,67:end) = 4;

mv = [mv(:) mv(:)];
y = mv + y;
x = mv + x;

N = ceil(0.2*m*n);

xplot = zeros(m*n,d); xplot(mask(:),:) = x;
%extract some observations
obs_ind = randperm(length(y)); obs_ind = obs_ind(1:N);
Ye = y(obs_ind,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

obs_plot = nan(size(y));
obs_plot(obs_ind,:) = Ye;

figure(1)
subplot(221)
imagesc(reshape(xplot(:,1),[m n 1])); colorbar;axis image;axis off
subplot(224)
imagesc(reshape(xplot(:,2),[m n 1])); colorbar;axis image;axis off
subplot(222)
h = imagesc(reshape(obs_plot(:,1),[m n]));
set(h,'alphadata',~isnan(reshape(obs_plot(:,1),[m n])));axis image;axis off
colorbar
subplot(224)
h = imagesc(reshape(obs_plot(:,2),[m n]));
set(h,'alphadata',~isnan(reshape(obs_plot(:,1),[m n])));axis image;axis off
colorbar

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Estimate LGFM models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K_max = 5;
theta_lgfm = cell(1,K_max);
z_lgfm = cell(1,K_max);
p_lgfm = cell(1,K_max);
krigLGFM = cell(1,K_max);
varLGFM = cell(1,K_max);
tLGFM = zeros(K_max,1);
mse_LGFM = zeros(K_max,2);
mae_LGFM = zeros(K_max,2);
qign_LGFM = zeros(K_max,2);


tic
theta_start = struct('logkappa2', log(0.001),...
                     'logsigma2', log(var(Ye)/100)', 'Qd', diag(1./var(Ye)), 'mu0', mean(Ye)');

opts = struct('car_order', car_order,'tol',1e-4,'Ntrace',50,...
              'step_rate',0.1,'step0',0.3,'learning_rate',0.5,'plot',1);
gtheta = gaussian_ecg(Ye,mask,obs_ind,theta_start,opts,1000);
tLGFM(1,2) = toc;
tic
[krigGauss, varGauss] = gaussian_krig(1,gtheta,Ye,mask,obs_ind);
tLGFM(1,3) = toc;
mse_LGFM(1,:) = mean((x-krigGauss).^2);
mae_LGFM(1,:) = mean(abs(x-krigGauss));
qign_LGFM(1,:) = mean(((x-krigGauss)./(2*sqrt(varGauss))).^2+log(sqrt(varGauss)));

opts_mrf = struct('gibbs_iter',10,'tol',1e-8,'alpha_type',0,'plot',0);
opts_init = struct('car_order', car_order,'tol',1e-5,'Ntrace',10,...
                    'step_rate',0.1,'step0',0.5,'plot',0,'learning_rate',0.2);
opts = struct('gibbs_iter',5,'tol',1e-4,'alpha_type',-1,'plot',2,...
          'Ntrace',20,'step_rate',0.1,'step0',0.5,'learning_rate',0);

for K=2:K_max
  %calculate starting values based on the MRF estimates
  time_init = tic;
  [theta,z,p,converged,alpha,beta]=mrf_ecg(Ye,K,mask,obs_ind,opts_mrf,50);
  [gauss_obj, z0] = lgfm_startvalue_from_mrf(Ye,p,theta,mask,obs_ind,opts_init,100);
opts.beta0 = beta;
  opts.alpha0 = alpha;
  tLGFM(K,1) = toc(time_init)
  time_est = tic;
  %estimate parameters
  [theta_lgfm{K}, z_lgfm{K}, p_lgfm{K}] = lgfm_ecg(Ye,mask,obs_ind, z0, gauss_obj,opts,1000);
  tLGFM(K,2) = toc(time_est)
  time_krig = tic;
  %compute kriging prediction
  [krigLGFM{K}, varLGFM{K}] = lgfm_kriging(z_lgfm{K}{1},theta_lgfm{K},1000,mask,obs_ind);
  tLGFM(K,3) = toc(time_krig)
  mse_LGFM(K,:) = mean((x-krigLGFM{K}).^2);
  mae_LGFM(K,:) = mean(abs(x-krigLGFM{K}));
  qign_LGFM(K,:) = mean(((x-krigLGFM{K})./(2*sqrt(varLGFM{K}))).^2+log(sqrt(varLGFM{K})));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c = [min(x(:,1)) max(x(:,1))];
figure(2)
subplot(241)
imagesc(reshape(krigGauss(:,1),[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)
subplot(242)
imagesc(reshape(krigLGFM{2}(:,1),[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)
subplot(243)
imagesc(reshape(krigLGFM{3}(:,1),[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)
subplot(244)
imagesc(reshape(krigLGFM{4}(:,1),[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)
subplot(245)

c = [min(x(:,2)) max(x(:,2))];
imagesc(reshape(krigGauss(:,2),[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)
subplot(246)
imagesc(reshape(krigLGFM{2}(:,2),[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)
subplot(247)
imagesc(reshape(krigLGFM{3}(:,2),[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)
subplot(248)
imagesc(reshape(krigLGFM{4}(:,2),[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)


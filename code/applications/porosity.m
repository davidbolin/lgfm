%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Load the data.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%The data used in this application can be downloaded from
%http://www.spe.org/web/csp/index.html

clearvars
close all

load spe_perm.dat
load spe_phi.dat
por = reshape(spe_phi',[60 220 85]);
perm = reshape(spe_perm',[60 220 85 3]);

Yperm = squeeze(log(perm(:,1:100,60,1))); %horizontal permeabillty
Ypor = squeeze(por(:,1:100,60)); %porosity


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Extract data by taking every kk number of pixels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%corsening number:
kk = 2;

% Number of classes
K = 2;
m = size(Yperm,1);
n = size(Yperm,2);
Y = [Yperm(:) Ypor(:)];

oi = zeros(m,n);
for i = 1:kk:m
  for j=1:kk:n
    oi(i,j) = 1;
  end
end
obs_ind = oi==1;
pred_ind = oi==0;
obs_ind = find(obs_ind(:));
pred_ind = find(pred_ind(:));

mask = true(m,n);
Ye = Y(obs_ind,:);
Yp = Y(pred_ind,:);

%add a small noise to zero-porosity to avoid numerical problems
Ye(Ye==0) = abs(1e-3*randn(sum(Ye(:)==0),1));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate MRF estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K_max = 10;
theta = cell(1,K_max);
z = cell(1,K_max);
p = cell(1,K_max);
alpha = cell(1,K_max);
beta = cell(1,K_max);
tMRF = zeros(K_max,1);
mse_MRF = zeros(K_max,2);
mae_MRF = zeros(K_max,2);
qign_MRF = zeros(K_max,2);
opts_mrf = struct('gibbs_iter',10,'tol',1e-8,'alpha_type',0,'plot',2);
for K=2:K_max
  tic
  [theta{K},z{K},p{K},converged,alpha{K},beta{K}]=mrf_ecg(Ye,K,mask,obs_ind,opts_mrf,200);
  [krigMRF, varMRF] = mrf_kriging(Ye,z{K}{1},theta{K},alpha{K},beta{K},5000,mask,obs_ind);
  tMRF(K) = toc;
  mse_mrf(K,:) = mean((Yp-krigMRF(pred_ind,:)).^2);
  mae_mrf(K,:) = mean(abs(Yp-krigMRF(pred_ind,:)));
  qign_mrf(K,:) = mean(((Yp-krigMRF(pred_ind,:))./(2*sqrt(varMRF(pred_ind,:)))).^2+log(sqrt(varMRF(pred_ind,:))));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate Gaussian estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta_lgfm = cell(1,K_max);
z_lgfm = cell(1,K_max);
p_lgfm = cell(1,K_max);
krigLGFM = cell(1,K_max);
varLGFM = cell(1,K_max);
tLGFM = zeros(K_max,2);
mse_LGFM = zeros(K_max,2);
mae_LGFM = zeros(K_max,2);
qign_LGFM = zeros(K_max,2);

car_order = 1;
tic
theta_start = struct('logkappa2', log(0.001),...
                     'logsigma2', log(var(Ye)/100)', 'Qd', diag(1./var(Ye)), 'mu0', mean(Ye)');

opts = struct('car_order', car_order,'tol',0,'fixed_trace',0,'Ntrace',5,...
              'step_rate',0.2,'step0',0.5,'learning_rate',0,'plot',1);
[gtheta, Xk] = gaussian_ecg(Ye,mask,obs_ind,theta_start,opts,1000);
tLGFM(1,1) = toc;
tic
[krigGauss, varGauss] = gaussian_krig(1,gtheta,Ye,mask,obs_ind);
tLGFM(1,2) = toc;
mse_LGFM(1,:) = mean((Yp-krigGauss(pred_ind,:)).^2);
mae_LGFM(1,:) = mean(abs(Yp-krigGauss(pred_ind,:)));
qign_LGFM(1,:) = mean(((Yp-krigGauss(pred_ind,:))./(2*sqrt(varGauss(pred_ind,:)))).^2+log(sqrt(varGauss(pred_ind,:))));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate LGFM estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opts_init = struct('car_order', car_order,'tol',1e-5,'Ntrace',5,...
                    'step_rate',0.1,'step0',0.2,'plot',1,'learning_rate',0);
opts = struct('gibbs_iter',4,'tol',1e-4,'alpha_type',-1,'plot',2,...
          'Ntrace',5,'step_rate',0.01,'step0',0.2,'learning_rate',0);

for K=4:K_max
  %calculate starting values based on the MRF estimates
  tic
  [gauss_obj, z0] = lgfm_startvalue_from_mrf(Ye,p{K},theta{K},mask,obs_ind,opts_init,200);
  opts.beta0 = beta{K};
  opts.alpha0 = alpha{K};
  %estimate parameters
  [theta_lgfm{K}, z_lgfm{K}, p_lgfm{K}] = lgfm_ecg(Y,mask,obs_ind, z0, gauss_obj,opts,5000);
  tLGFM(K,1) = toc;
  tic
  %compute kriging prediction
  [krigLGFM{K}, varLGFM{K}] = lgfm_kriging(z_lgfm{K}{1},theta_lgfm{K},1000,mask,obs_ind);
  tLGFM(K,2) = toc;
  mse_LGFM(K,:) = mean((Yp-krigLGFM{K}(pred_ind,:)).^2);
  mae_LGFM(K,:) = mean(abs(Yp-krigLGFM{K}(pred_ind,:)));
  qign_LGFM(K,:) = mean(((Yp-krigLGFM{K}(pred_ind,:))./(2*sqrt(varLGFM{K}(pred_ind,:)))).^2+log(sqrt(varLGFM{K}(pred_ind,:))));
end



figure(1)
for i=1:2
  subplot(2,3,1+(i-1)*3)
  plot(2:K_max,sqrt(mse_mrf(2:end,i)),'LineWidth',2)
  hold on
  plot(1:K_max,sqrt(mse_LGFM(:,i)),'LineWidth',2)
  ax = gca;
  ax.XTick = 1:10;
  legend('MRF models','LGMF models')
  xlabel('K')
  ylabel('RMSE')
end


for i=1:2
  subplot(2,3,2+(i-1)*3)
  plot(2:K_max,mae_mrf(2:end,i),'LineWidth',2)
  hold on
  plot(1:K_max,mae_LGFM(:,i),'LineWidth',2)
  ax = gca;
  ax.XTick = 1:K_max;
  legend('MRF models','LGMF models')
  xlabel('K')
  ylabel('MAE')
end

for i=1:2
  subplot(2,3,3+(i-1)*3)
  plot(2:K_max,qign_mrf(2:end,i),'LineWidth',2)
  hold on
  plot(1:K_max,qign_LGFM(:,i),'LineWidth',2)
  ax = gca;
  ax.XTick = 1:K_max;
  legend('MRF models','LGMF models')
  xlabel('K')
  ylabel('QIGN')
end

clearvars
close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Simulate data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m = 60;
n = 100;
K = 3;
d = 1;

N = ceil(0.33*m*n);
mask = true(m,n);
car_order = 2;
beta = 1*ones(1,K);
alpha = ones(1,K)/K;

theta = cell(1,K);

kp2 = 0.01;
for i=1:K
  theta{i}.Qd = 2*i;
  theta{i}.kappa2 = kp2;
  theta{i}.mu = 2*i*(1:d)';
  theta{i}.order = car_order;
end
sigma_e = ones(1,d)*sqrt(0.05);

[y, x, z] = lgfm_gendat(mask,alpha,beta,theta,sigma_e);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot data and extract observations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zplot = zeros(m*n,K); zplot(mask(:),:) = z;
xplot = zeros(m*n,d); xplot(mask(:),:) = x;
%extract some observations
obs_ind = randperm(length(y)); obs_ind = obs_ind(1:N);
Ye = y(obs_ind,:);
obs_plot = nan(size(y));
obs_plot(obs_ind,:) = Ye;

figure(1);
subplot(131)
imagesc(rgbimage(reshape(zplot,[m n 3])));axis image
subplot(132)
imagesc(reshape(xplot(:,1),[m n 1])); colorbar;axis image
subplot(133)
h = imagesc(reshape(obs_plot(:,1),[m n]));
set(h,'alphadata',~isnan(reshape(obs_plot(:,1),[m n])));axis image
colorbar

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate LGFM estimates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K_max = 5;
theta_lgfm = cell(1,K_max);
z_lgfm = cell(1,K_max);
p_lgfm = cell(1,K_max);
krigLGFM = cell(1,K_max);
varLGFM = cell(1,K_max);
tLGFM = zeros(K_max,3);
mse_LGFM = zeros(K_max,1);
mae_LGFM = zeros(K_max,1);
qign_LGFM = zeros(K_max,1);


tic
theta_start = struct('logkappa2', log(0.001),...
                     'logsigma2', log(var(Ye)/100)', 'Qd', diag(1./var(Ye)), 'mu0', mean(Ye)');

opts = struct('car_order', car_order,'tol',1e-4,'Ntrace',50,...
              'step_rate',0.1,'step0',0.5,'learning_rate',0.5,'plot',1);
gtheta = gaussian_ecg(Ye,mask,obs_ind,theta_start,opts,1000);
tLGFM(1,2) = toc;
tic
[krigGauss, varGauss] = gaussian_krig(1,gtheta,Ye,mask,obs_ind);
tLGFM(1,3) = toc;
mse_LGFM(1,:) = mean((x-krigGauss).^2);
mae_LGFM(1,:) = mean(abs(x-krigGauss));
qign_LGFM(1,:) = mean(((x-krigGauss)./(2*sqrt(varGauss))).^2+log(sqrt(varGauss)));


opts_mrf = struct('gibbs_iter',10,'tol',1e-8,'alpha_type',0,'plot',0);
opts_init = struct('car_order', car_order,'tol',1e-5,'Ntrace',25,...
                    'step_rate',0.1,'step0',0.5,'plot',0,'learning_rate',0.2);
opts = struct('gibbs_iter',5,'tol',1e-4,'alpha_type',-1,'plot',2,...
          'Ntrace',20,'step_rate',0.2,'step0',1,'learning_rate',0.5);

for K=2:K_max
  %calculate starting values based on the MRF estimates
  time_init = tic;
  [theta,z,p,converged,alpha,beta]=mrf_ecg(Ye,K,mask,obs_ind,opts_mrf,50);
  [gauss_obj, z0] = lgfm_startvalue_from_mrf(Ye,p,theta,mask,obs_ind,opts_init,100);
opts.beta0 = beta;
  opts.alpha0 = alpha;
  tLGFM(K,1) = toc(time_init);
  time_est = tic;
  %estimate parameters
  [theta_lgfm{K}, z_lgfm{K}, p_lgfm{K}] = lgfm_ecg(Ye,mask,obs_ind, z0, gauss_obj,opts,1000);
  tLGFM(K,2) = toc(time_est);
  time_krig = tic;
  %compute kriging prediction
  [krigLGFM{K}, varLGFM{K}] = lgfm_kriging(z_lgfm{K}{1},theta_lgfm{K},1000,mask,obs_ind);
  tLGFM(K,3) = toc(time_krig);
  mse_LGFM(K,:) = mean((x-krigLGFM{K}).^2);
  mae_LGFM(K,:) = mean(abs(x-krigLGFM{K}));
  qign_LGFM(K,:) = mean(((x-krigLGFM{K})./(2*sqrt(varLGFM{K}))).^2+log(sqrt(varLGFM{K})));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure(2)
subplot(231)
imagesc(reshape(xplot(:,1),[m n]));
c = caxis;
imagesc(reshape(krigGauss,[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)
subplot(234)
imagesc(reshape(krigLGFM{3},[m n]));
colorbar();axis image;colormap('default');axis off
caxis(c)

c = full([min(min(sqrt(varGauss)),min(sqrt(varLGFM{3}))),...
          max(max(sqrt(varGauss)),max(sqrt(varLGFM{3})))]);
subplot(232)
imagesc(reshape(sqrt(varGauss),[m n]));
colorbar();axis image;colormap('copper');axis off
caxis(c)
subplot(235)
imagesc(reshape(sqrt(varLGFM{3}),[m n]));
colorbar();axis image;colormap('copper');axis off
caxis(c)

d1 = xplot(:,1)-krigGauss;
d2 = xplot(:,1)-krigLGFM{3};
c = full([0,max(max(abs(d1)),max(abs(d2)))]);

subplot(233)
imagesc(reshape(abs(d1),[m n]));
colorbar();axis image;colormap('copper');axis off
caxis(c)
subplot(236)
imagesc(reshape(abs(d2),[m n]));
colorbar();axis image;colormap('copper');axis off
caxis(c)


figure(3)
subplot(131)
plot(1:K_max,sqrt(mse_LGFM),'LineWidth',2)
ax = gca;
ax.XTick = 1:K_max;
xlabel('K')
ylabel('RMSE')
subplot(132)
plot(1:K_max,mae_LGFM,'LineWidth',2)
ax = gca;
ax.XTick = 1:K_max;
xlabel('K')
ylabel('MAE')
subplot(133)
plot(1:K_max,qign_LGFM,'LineWidth',2)
ax = gca;
ax.XTick = 1:K_max;
xlabel('K')
ylabel('QIGN')

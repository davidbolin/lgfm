function [y, x, z] = lgfm_gendat(mask,alpha,beta,theta,sigma_e)
%
% [y, x, z, X] = lgfm_gendat(mask,alpha,beta,theta,sigma_e) samples from
% a LGFM model.
% Inputs:
% mask : an m x n matrix specifying a domain of interest on a regular
% m-by-n grid. the values of mask should be 1 or 0, where 0 indicates that
% this part of the grid is not used.
% alpha and beta : the parameters of the discrete MRF.
% theta : a cell with parameters of the (multivariate) Gaussian fields with
% a kronecker covariance structure, r(s,t) = inv(Qd)xrho(|s-t|), where rho
% is the covariance function for a CAR(p) model.
%            mu : the mean value
%            Qd : the precision matrix for the multivariate part.
%            order : the order of the CAR model.
% sigma_e : the standard deviation of the measurement noise.
%
% Outputs:
% y : the data
% x : the latent field
% z : the discrete MRF.
%

if islogical(mask)==0; mask = logical(mask); end
if issparse(mask)==0; mask = sparse(mask); end

%Exatract parameters
K = length(theta);
d = size(theta{1}.Qd,1);
[m, n] = size(mask);

%Simulate discrete MRF
N = [0 1 0; 1 0 1; 0 1 0];
W = build_W(N,mask);
z0 = zeros(sum(mask(:)),K);
if K>1
	z = mrf_sim_grad(z0,N,log(alpha),beta,100,mask,W,alpha);
	if iscell(z); z = z{1}; end
else
	z = ones(size(z0));
end

X = zeros(n*d,K);
Z = X;
G = stencil2prec(mask,[0 -1 0;-1 4 -1;0 -1 0]);
C = speye(m*n);
C = C(mask(:),mask(:));
nd = sum(mask(:));
for i=1:K
  if theta{i}.order == 1
    Qk = (G + theta{i}.kappa2*C);
  elseif theta{i}.order == 2
    KK = G + theta{i}.kappa2*C;
    Qk = KK'*KK;%/(4*pi*theta{i}.kappa2);
  else
    error('not implemented for CAR(p) with p>2.')
  end
  Q = kron(theta{i}.Qd ,Qk);
	p = amd(Q);
  R = chol(Q(p,p));
	Z(p,i) = R\randn(d*nd,1);
end

x = zeros(nd*d,1);
B = kron(eye(d),ones(nd,1));
for i=1:K
	x = x + repmat(z(:,i),[d 1]).*(B*theta{i}.mu+Z(:,i));
end

y = x + kron(diag(sigma_e),speye(nd))*randn(nd*d,1);


y = reshape(y,[nd d]);
x = reshape(x,[nd d]);








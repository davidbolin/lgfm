function [par_ests,z,p,converged]=lgfm_ecg(y,mask,obs_ind,z,gauss_obj,opts,iter)

%generate z and gauss_obj with lgfm_generate_startvalue.

% opts    : Structure with various options:
%           alpha_type   : -1 no alpha parameters, 1 different alpha
%           sigma_fixed  : Use this value as a known value for the
%                          measurement noise variance.
%           gibbs_iter   : number of iterations in Gibbs sampler.
%           plot         : Plot parameter tracks?
%           Ntrace       : Number of MC samples for trace estimation.
%           tol          : Tolerance for convergence.
%           step_rate    : scaling of step size iter^(-step_rate)
%           step0        : initial step size
%           beta0        : starting value for beta.
%           alpha0        : starting value for alpha.
%           learning_rate: learning rate for stochcastic gradients.

if nargin < 7; iter = 20; end
if nargin < 6; opts = []; end

K = length(gauss_obj.obj);
if isempty(opts)
  alpha_type = 1;
  sigma_fixed = 0;
  plotflag = 0;
  tol = 1e-5;
  step_rate = 0.1;
  n_trace = 10;
  gibbs_iter = 10;
  learning_rate = 0.5;
  step0 = 0.1;
  beta = 0.1*ones(1,K);
  alpha = log(ones(1,K)/K);
else
  names = fieldnames(opts);
  if sum(strcmp(names,'sigma_fixed'))>0
    sigma_fixed = opts.sigma_fixed;
    if size(sigma_fixed,1)<size(sigma_fixed,2)
      sigma_fixed = sigma_fixed';
    end
  else
    sigma_fixed = 0;
  end
  if sum(strcmp(names,'alpha_type'))>0
    alpha_type = opts.alpha_type;
  else
    alpha_type = 1;
  end
  if sum(strcmp(names,'learning_rate'))>0
    learning_rate = opts.learning_rate;
  else
    learning_rate = 0.5;
  end
  if sum(strcmp(names,'plot'))>0
    plotflag = opts.plot;
  else
    plotflag = 1;
  end
  if sum(strcmp(names,'beta0'))>0
    beta = opts.beta0;
  else
    beta = 0.1*ones(1,K);
  end
  if sum(strcmp(names,'alpha0'))>0
    alpha = opts.alpha0;
  else
    alpha = log(ones(1,K)/K);
  end
  if sum(strcmp(names,'step0'))>0
    step0 = opts.step0;
  else
    step0 = 0.1;
  end
  if sum(strcmp(names,'Ntrace'))>0
    n_trace = opts.Ntrace;
  else
    n_trace = 10;
  end
  if sum(strcmp(names,'gibbs_iter'))>0
    gibbs_iter = opts.gibbs_iter;
  else
    gibbs_iter = 10;
  end
  if sum(strcmp(names,'tol'))>0
    tol = opts.tol;
  else
    tol = 1e-5;
  end
  if sum(strcmp(names,'step_rate'))>0
    step_rate = opts.step_rate;
  else
    step_rate = 0.1;
  end
end

if isempty(obs_ind), obs_ind = ones(size(z,1),1); end

if iscell(y);
	nrep = length(y);
else
	nrep = 1;
	y  = {y};
	if ~iscell(z); z = {z}; end
	if ~iscell(mask); mask = {mask}; end
	if length(y) ~= length(mask)
		error('A mask must be supplied for each image.')
	end
end

if isempty(obs_ind)
	for i=1:nrep
		obs_ind{i} = ones(size(z{i},1),1);
	end
else
	if ~iscell(obs_ind); obs_ind = {obs_ind}; end
end


if plotflag;
	hFig = figure(),clf;
	if plotflag>1; hFig2 = figure();clf; end
	if plotflag>2; hFig3 = figure();clf; end
end
cl = cell(nrep,1);
for rep=1:nrep
	[m{rep} n{rep} l{rep}] = size(mask{rep});
	sz{rep} = size(y{rep}); mn{rep} = sum(mask{rep}(:));
	d = sz{rep}(end);
	K = size(z{rep},2);
	if length(sz{rep}) == 3; y{rep} = colstack(y{rep}); end
	mc{rep} = colstack(mask{rep});
    cl{rep} = zeros(1,mn{rep});
    for i=1:mn{rep}
        cl{rep}(i) = find(z{rep}(i,:));
    end
end


N = [0 1 0; 1 0 1; 0 1 0];
for rep = 1:nrep
  if islogical(mask{rep})==0; mask{rep} = logical(mask{rep}); end
  if issparse(mask{rep})==0; mask{rep} = sparse(mask{rep}); end
  W{rep} = build_W(N,mask{rep});
end


%change to common sigma
sigmas = zeros(K,d);
for kk=1:K; sigmas(kk,:) = gauss_obj.obj{kk}.sigma2; end
sigmacommon = mean(sigmas);
for kk=1:K;	gauss_obj.obj{kk}.sigma2 = sigmacommon'; end

fprintf('. starting optmization\n')
if plotflag
  alpha_history(1,:) = exp(alpha)/sum(exp(alpha));
  beta_history(1,:) = beta;
  for k=1:K
    mu_history(k,:,1) = gauss_obj.obj{k}.mu;
    Sigma_history(:,k,1) = gauss_obj.obj{k}.Qd(:);
    kappa_history(k,1) = gauss_obj.obj{k}.kp2;
    sig2_history(:,k,1) = gauss_obj.obj{k}.sigma2;
  end
  set(0,'CurrentFigure',hFig);
	for rep = 1:nrep
		subplot(1,nrep,rep)
    clplot = zeros(m{rep}*n{rep},1);
    clplot(mc{rep}) = cl{rep};
    imagesc(reshape(clplot,[m{rep} n{rep}]))
    drawnow
  end
end
loop = 0; done = 0;
for rep = 1:nrep; ngauss{rep} = size(gauss_obj.C{rep},1); end

pstack = [];
for rep = 1:nrep; pstack = [pstack;z{rep}]; end

%%%%%%%%%%%%%%%%%%%%%%
%burnin
%%%%%%%%%%%%%%%%%%%%%%%
trace_obj = build_trace_obj(ngauss,d,K,n_trace);
[z, Mz, EX] = lgfm_sim_grad(z, gauss_obj, beta, 10, mask, W, alpha, [alpha_type 0], trace_obj,obs_ind);

%%%%%%%%%%%%%%%%%%%%%%
% start estimation
%%%%%%%%%%%%%%%%%%%%%%%

loop = 0;
gradient_old = 0;
while (~done)
	loop = loop+1;
  gamma = step0*loop^(-step_rate);
  p_old = pstack;
  fprintf('%d : ',loop)

  %%%%%%%%%%%%%%%%%%%%%%%%%%
  % Calculate gradients
  %%%%%%%%%%%%%%%%%%%%%%%%%
  fprintf('sim grad ')
  tic
  trace_obj = update_trace_obj(trace_obj);
  [z, p, EX, da, db, gN, trace_obj, H, gH] = lgfm_sim_grad(z, gauss_obj, beta, ...
              gibbs_iter, mask, W, alpha, [alpha_type 0], trace_obj,obs_ind);

  gtime = toc;
	fprintf('(%f), ',gtime)

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Update Gaussian parameters
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  fprintf('take step')
  tic

  %change to common sigma:
		sigmaN = zeros(d,1);
	sigmaH = zeros(d,d);
	sigmaHb = [];
	npars = size(gH{kk},1);
	Pold = log(gauss_obj.obj{1}.sigma2);
	for kk=1:K
	  if(sigma_fixed==0)
  		sigmaN = sigmaN + gN{kk}(d+1:2*d);
  	end
		sigmaH = sigmaH + gH{kk}(d+1:2*d,d+1:2*d);
		sigmaHb = [sigmaHb gH{kk}(1:d,d+1:2*d) zeros(d,npars-2*d)];
		Pold = [Pold; gauss_obj.obj{kk}.P(1:d);gauss_obj.obj{kk}.P(2*d+1:end)];
	end

	c_grad = sigmaN;
	c_hess = sigmaH;

	for kk=1:K
		c_grad = [c_grad;
              gN{kk}(1:d);
              gN{kk}(2*d+1:end)];
		c_hess = blkdiag(c_hess,gH{kk}(1:d,1:d),gH{kk}(2*d+1:end,2*d+1:end));
	end
	c_hess(1:d,d+1:end) = sigmaHb;
	c_hess(d+1:end,1:d) = sigmaHb';

  c_pre = diag(1./diag(c_hess));

  %only update parameters where gradient is non-zero
  pre_grad = c_pre*c_grad;
  pre_hess = c_pre*c_hess;
  i_up = abs(c_grad) >1e-15;
  new_step = zeros(size(c_grad));
  new_step(i_up) = pre_hess(i_up,i_up)\pre_grad(i_up);

  gradient = learning_rate*gradient_old + new_step;
  P = Pold - gamma*gradient;
  stepsize_gauss = (gamma*gradient)'*c_grad;
  %limit step by size of parameters for stability
  pind = abs(P-Pold)>2*abs(Pold);
  if any(isnan(P))
    pind = true(size(pind));
    fprintf(' (nan) ') %should not be here.
  end
  if sum(pind)>0 && length(gradient_old)>1
    P(pind) = 1.1*Pold(pind);
    gradient(pind)  = 0;%gradient_old(pind);
    fprintf(' (limit)')
  end

  gradient_old = gradient;
  if(sigma_fixed)
    P(1:d) = Pold(1:d);
  end
  for kk=1:K
    Pk = P([d+1:2*d 1:d 2*d+1:npars]);
    EXk = cell(1,nrep);
    for rep = 1:nrep; EXk{rep} = EX{rep,kk}; end
    %do regression on beta%%%%%%%%%%
    %tmp = 0;
    %for rep = 1:nrep
    %  tmp = tmp + mean(EXk{rep})/nrep;
    %end
    %Pk(1:d) = Pk(1:d) + tmp';
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    gauss_obj.obj{kk} = lgfm_update_gauss(Pk,kk,gauss_obj,EXk);
    P = [P(1:d);P(npars+1:end)];
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Update MRF parameters
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if alpha_type == 1
    grad_mrf = [da(2:end) db]';
    H_mrf = H(2:end,2:end);
  else
    H_mrf = H;
    grad_mrf = [da db]';
  end
  c_pre = diag(1./diag(H_mrf));
  step = (c_pre*H_mrf)\(c_pre*grad_mrf);

  %limit stepsize for numerical stability
  step = gamma*step;
  if alpha_type==1
  stepsize_mrf = step'*[da(2:end) db]';
  else
    stepsize_mrf = step*[da db]';
  end
  stepsize = abs(stepsize_gauss) + abs(stepsize_mrf);

	if alpha_type == 1
  	alpha_old = alpha;
		alpha(2:K) = alpha(2:K) + step(1:K-1)';
		pind = abs(alpha-alpha_old)>abs(alpha_old);
		if sum(pind)>0
      alpha(pind) = 1.1*alpha_old(pind);
      fprintf(' (limit)')
    end
    bstep = step(K:end);
	else
		bstep = step;
	end
  %limit for numerical stability
  beta_old = beta;
  beta =   beta + bstep';
  pind = abs(beta-beta_old)>abs(beta_old);
		if sum(pind)>0
    beta(pind) = 1.1*beta_old(pind);
    fprintf(' (limit)')
  end
	beta = max(min(beta,20),-20);
	gtime = toc;
	fprintf(' (%f), stepsize %f\n',gtime,stepsize)

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Check stopping criterion
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  pstack = [];
  for rep = 1:nrep; pstack = [pstack;p{rep}]; end
 	p_diff(loop,1) = max(abs((p_old(:)-pstack(:))));
 	p_diff(loop,2) = abs(stepsize);
  converged = (loop>1) & (p_diff(loop,2)<tol);
  done = converged | (loop>=iter);

  %%%%%%%%%%%%%%%%%%%%%%%%%%
  % Plot parameter tracks
  %%%%%%%%%%%%%%%%%%%%%%%%%
  if plotflag
    alpha_history(loop+1,:) = exp(alpha)/sum(exp(alpha));
    beta_history(loop+1,:) = beta;
    for k=1:K
      mu_history(k,:,loop+1) = gauss_obj.obj{k}.mu;
      Sigma_history(:,k,loop+1) = gauss_obj.obj{k}.Qd(:);
      kappa_history(k,loop+1) = gauss_obj.obj{k}.kp2;
      sig2_history(:,k,loop+1) = gauss_obj.obj{k}.sigma2;
    end
    set(0,'CurrentFigure',hFig);
    clf
    for rep = 1:nrep
      subplot(1,nrep,rep)
      [tmp,cl] = max(p{rep},[],2);
      clplot = zeros(m{rep}*n{rep},K); clplot(mc{rep},:) = p{rep};
      imagesc(rgbimage(reshape(clplot,[m{rep} n{rep} K]),jet(K)))
      axis image
    end

    if plotflag>1;
      set(0,'CurrentFigure',hFig2);
      subplot(K+1,3,1); plot(alpha_history); title('alpha')
      subplot(K+1,3,2); plot(squeeze(beta_history)); title('beta')
      subplot(K+1,3,3); plot(squeeze(sig2_history(:,1,:))'); title('sigma2')
      ip = 1;
      for k=1:K
        subplot(K+1,3,ip+3);plot(squeeze(mu_history(k,:,:))'); title('mu')
        subplot(K+1,3,ip+4); plot(squeeze(Sigma_history(:,k,:))'); title('Qd')
        subplot(K+1,3,ip+5);plot(kappa_history(k,:)'); title('kappa2')
        ip = ip+3;
      end
    end
    if plotflag>2;
      set(0,'CurrentFigure',hFig3); subplot(211);
      lim = p_diff(loop);
      p_diff_n = histc(p_old(:)-pstack(:),...
          					 linspace(-lim,lim,100))*100/length(pstack(:));
      bar(linspace(-lim,lim,100),p_diff_n.^0.25,'histc');
      axis([-lim,lim,0,max(p_diff_n).^0.25])
      subplot(212); plot(log10(p_diff(:,2)));
    end
    drawnow
	end
end


par_ests = struct;
par_ests.alpha = alpha;
par_ests.beta = beta;
par_ests.obj = cell(1,K);
par_ests.gauss_obj = gauss_obj;
for i=1:K
	par_ests.obj{i}.kappa2 = gauss_obj.obj{i}.kp2;
	par_ests.obj{i}.sigma2 = gauss_obj.obj{i}.sigma2;
	par_ests.obj{i}.Qd = gauss_obj.obj{i}.Qd;
	par_ests.obj{i}.mu = gauss_obj.obj{i}.mu;
end

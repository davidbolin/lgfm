function [EX,VX, Mz, z] = lgfm_kriging(z,theta,iter,mask,obs_ind)

    gauss_obj = theta.gauss_obj;
    alpha0 = theta.alpha;
    beta = theta.beta;
    d = size(gauss_obj.Y{1},2);
    zr = round(z);
    if norm(zr - z)>1e-5
        warning('z should be an indicator field')
    end
    z = zr;

    
    N = [0 1 0; 1 0 1; 0 1 0];
    if islogical(mask)==0; mask = logical(mask); end
    if issparse(mask)==0; mask = sparse(mask); end
    W = build_W(N,mask);
    

    a=2; b=2; 
    [m, n] = size(mask);
    ij_ = [kron(1:a,ones(1,b)); kron(ones(1,a),1:b)];
    
    cmask = mask(:);
    [mn,K] = size(z);

    trace_obj = build_trace_obj(size(gauss_obj.C,1),size(gauss_obj.obj{1}.Qd,1),K,20);

    a0 = make_K_im(exp(alpha0),[mn K]);
    b0 = make_K_im(beta,[mn K]);

    Mz = zeros(mn,K);
    KK = repmat(reshape(1:K,[1,K]),[mn,1]);
    on = ones(1,K);

    %precalculate masks:
    for k=1:a*b
      [I, J] = meshgrid(ij_(1,k):a:m,ij_(2,k):b:n);
      ijmask = sparse(I,J,true,m,n);
      ijm(:,k) = ijmask(cmask);  
    end

    %change to checkerboard updates:
    ijmij = ijm;
    clear ijm;
    ijm(:,1) = logical(ijmij(:,1) + ijmij(:,4));
    ijm(:,2) = logical(ijmij(:,2) + ijmij(:,3));
    

    Wij = cell(1,2); 	b0ij = cell(1,2);	eaij = cell(1,2);
    KKij = cell(1,2);	ijmf = cell(1,2);	Mzc = cell(1,2);
    for k=1:2
        Wij{k} = W(ijm(:,k),:);		b0ij{k} = b0(ijm(:,k),:);
        KKij{k} = KK(ijm(:,k),:);	Mzc{k} = zeros(sum(ijm(:,k)),K);
    end

    burn_in = 1;%max(floor(iter/10),1);
    n_save = iter - burn_in+1;
    reverseStr = '';

    % hFig1 = figure();
    % hFig2 = figure();
    EX = [];
    VX = [];
    for loop=1:iter
        msg = sprintf('Processed: %.02f %s', 100*loop/iter,'%%');
        fprintf([reverseStr, msg]);
        reverseStr = repmat(sprintf('\b'), 1, length(msg)-1);
        
        %sample Gaussian field
        [EXi,VXi,alpha_sim] = lgfm_sim_gauss({round(z)}, gauss_obj, trace_obj,{obs_ind},1);
        alpha_sim = alpha_sim{1};
        
        alpha = log(a0)+alpha_sim;
        
        %only save samples after burn-in
        if loop>burn_in
          ei = bsxfun(@times,z(:,1),bsxfun(@plus,gauss_obj.obj{1}.mu',reshape(EXi{1,1},[mn d])));
          vi = bsxfun(@times,z(:,1),reshape(VXi{1,1},[mn d]));
          for k = 2:length(EXi)
            ei = ei + bsxfun(@times,z(:,k),bsxfun(@plus,gauss_obj.obj{k}.mu',reshape(EXi{1,k},[mn d])));
            vi = vi + bsxfun(@times,z(:,k),reshape(VXi{1,k},[mn d]));
          end
            EX = [EX ei(:)];
            VX = [VX vi(:)];
        end
        
        %sample discrete MRF
        for k=1:2
          f = Wij{k}*z;
          Mz_cond = exp(alpha(ijm(:,k),:)).*exp(f.*b0ij{k});
          Msum = sum(Mz_cond,2);
          Mz_cond = Mz_cond./Msum(:,on);
          if loop > burn_in
            Mzc{k} = Mzc{k}+Mz_cond;
          end
          e = rand(sum(ijm(:,k)),1);
          x = 1 + sum( cumsum(Mz_cond,2) < e(:,on) , 2);
          z(ijm(:,k),:) = (x(:,on)==KKij{k})*1;
        end
        
    end
    fprintf('\n');

     VX = mean(VX,2) + var(EX,[],2);
     EX = mean(EX,2);
     EX = reshape(EX,[mn d]);
     VX = reshape(VX,[mn d]);
     
             
    for k=1:2
        Mz(ijm(:,k),:) = Mzc{k};
    end
    Mz = Mz/n_save;
    
end

function im=make_K_im(v,sz)
    if (length(v)==1) %v = 1x1
      im = v*ones(sz);
    elseif ((size(v,1)==sz(1)) && (size(v,2)==sz(2))) %v=mn x K, do nothing
        im = v;
    else %v = 1xK
      im = repmat(v,[sz(1),1]);
    end
end
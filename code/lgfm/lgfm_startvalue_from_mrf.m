function [gauss_obj, z] = lgfm_startvalue_from_mrf(Y,p,theta,mask,obs_ind,opts,iter)

if nargin < 7; iter = 30; end

if iscell(Y);
	nrep = length(Y);
  if ~iscell(mask) || ~iscell(obs_ind)
    error('mask and obs_ind should be cells if Y is.')
  end
else
	nrep = 1;
  Y  = {Y}; 
  mask = {mask};
  obs_ind = {obs_ind};
end
z = cell(1,nrep);
z0 = cell(1,nrep);
for rep = 1:nrep;
  [tmp,cl]=max(p{rep},[],2);
  z{rep} = zeros(size(p{rep}));
  for i=1:size(z{rep},1)
    z{rep}(i,cl(i)) = 1; 
  end
  z0{rep} = z{rep}(obs_ind{rep},:);
end

kp0 = 8/(0.5*size(mask{rep},1))^2; %starting value for kappa

gauss_obj = lgfm_generate_startvalue_gauss(z0,Y,mask,obs_ind,theta,iter,kp0,opts);


function gauss_obj = lgfm_generate_startvalue_gauss(z,Y,mask,obs_ind,theta,iter,kp0,opts)

if iscell(Y);
	nrep = length(Y);
else
	nrep = 1;
	Y  = {Y};	
	z = {z};
end

%build CAR matrices:
C = cell(1,nrep);
G = cell(1,nrep);
A = cell(1,nrep);
for i=1:nrep
  G{i} = stencil2prec(mask{i},[0 -1 0;-1 4 -1;0 -1 0]);
  C{i} = speye(numel(mask{i}));
  C{i} = C{i}(mask{i}(:),mask{i}(:));
  A{i} = C{i}(obs_ind{i},:);
end


gauss_obj = build_gauss_obj(z,G,C,A,Y,theta,kp0,1,opts.car_order);
for rep = 1:nrep
	[mn{rep} K] = size(z{rep});
end

d = size(Y{1},2);
obj = cell(1,K);
for kk=1:K
  for rep = 1:nrep
    ik = z{rep}(:,kk)==1;
		Yest{rep} = Y{rep}(ik,:);
		Aest{rep} = A{rep}(ik,:);
    
    obs_ind_est{rep} = obs_ind{rep}(ik);
	end
	p0 = struct;
	p0.mu0 = theta{kk}.mu';
	Qd = inv(theta{kk}.Sigma);
	p0.Qd = Qd;
	p0.logkappa2 = log(kp0);
	p0.logsigma2 = log(var(Yest{rep})/100)';

	[gtheta, Xk] = gaussian_ecg(Yest,mask,obs_ind_est,p0,opts,iter);
	obj{kk} = lgfm_update_gauss(gtheta.P,kk,gauss_obj,Xk);
end
gauss_obj.obj = obj;
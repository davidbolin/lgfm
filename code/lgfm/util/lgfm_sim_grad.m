function [z, Mz, EX, da, db, Nabla_g, trace_obj, H, H_g] = lgfm_sim_grad(z, gauss_obj, beta, iter, mask, W, alpha0, opt, trace_obj,obs_ind)
% simulate the lgfm model and compute hessian and gradients of the
% likelihood using Gibbs sampling
% Inputs: 
%  z : starting value for the MRF
%  gauss_obj : structure containing the matrices for the Gaussian fields.
%  beta : the interaction parameter for the MRF.
%  iter : the number of iterations to run the Gibbs sampler for.
%  mask : a mask specifying the spatial domain.
%  W : A matrix containing the neighborhood structure for the MRF
%  alpha0 : the alpha parameter for the MRF.
%  opt : structure with options (FIX).
%  trace_obj : a structure containing the matrices for the trace approximation.
%  obs_ind : a vector with indices for the locations that are observed.
%
% Outputs:
% z : simulated MRF
% Mz : mean of the MRF simulations 
% EX : expected values of the Gaussian fields
% da, db, Nabla_g : gradients
% trace_obj : updated trace object 
% H, H_g : hessians

calc_grad = 0;
if nargout > 4
  calc_grad = 1;
end
calc_hess = 0;
if nargout > 8
  calc_hess = 1;
end

if nargin < 9; opt = []; end
if nargin < 8; alpha0 = []; end
if nargin < 7; error('must supply W'); end
if isempty(opt) && calc_grad == 1
	error('must supply opt if calculating gradients')
end
if isempty(alpha0) && calc_grad == 1
	error('must supply alpha if calculating gradients')
end
if isempty(opt); opt = [0 0 0]; end

if iscell(z); 
	nrep = length(z);
	if ~iscell(mask); mask = {mask}; end
	if ~iscell(W); W = {W}; end
else
	nrep = 1;
	z  = {z};
	if ~iscell(mask); mask = {mask}; end
	if ~iscell(W); W = {W}; end
end 

if isempty(obs_ind)
	for i=1:nrep
		obs_ind{i} = ones(size(z{i},1),1);
	end
end

for rep = 1:nrep
  [m{rep}, n{rep}] = size(mask{rep});
end

cmask{rep} = mask{rep}(:);
[mn{rep},K] = size(z{rep});

a0{rep} = make_K_im(exp(alpha0),[mn{rep} K]);
b0{rep} = make_K_im(beta,[mn{rep} K]);
Mz{rep} = zeros(mn{rep},K);
KK{rep} = repmat(reshape(1:K,[1,K]),[mn{rep},1]);

on = ones(1,K);

ijm = cell(nrep,1);
for rep = 1:nrep
	%precalculate masks:
  tmp = repmat(eye(2),m{rep},n{rep}); tmp = tmp(1:m{rep},1:n{rep});
	tmp = tmp(:); tmp = tmp(cmask{rep});
	ijm{rep}(:,1) = (tmp == 1);
	ijm{rep}(:,2) = (tmp == 0);
end


for rep = 1:nrep
	for k=1:2
		Wij{rep,k} = W{rep}(ijm{rep}(:,k),:);
		b0ij{rep,k} = b0{rep}(ijm{rep}(:,k),:);
		ea0ij{rep,k} = a0{rep}(ijm{rep}(:,k),:);
		KKij{rep,k} = KK{rep}(ijm{rep}(:,k),:);
		Mzc{rep,k} = zeros(sum(ijm{rep}(:,k)),K);
	end
end


for rep = 1:nrep
	observations = zeros(m{rep}*n{rep},1);
	observations(obs_ind{rep}) = 1;
	for k=1:2
		oind{rep,k} = find(observations(ijm{rep}(:,k)));
	end
end

if calc_grad == 1
  Nabla_g = cell(rep,K);
  for i=1:K
    np = size(gauss_obj.obj{i}.P,1);
    for rep = 1:nrep
      Nabla_g{rep,i} = zeros(np,1);
    end
  end
  if opt(1) == 1
    da = zeros(1,K);
    if opt(2)==1 %alpha vector and beta vector
      H = zeros(2*K,2*K);
      db = zeros(1,K);
    else %alpha vector and common beta
      H = zeros(K+1,K+1);
      db = 0;
    end
  else 
    da = [];
    if opt(2) == 1 %no alpha and beta vector
      H = zeros(K,K);
      db = zeros(1,K);
    else %no alpha and common beta
      H = 0;
      db = 0;
    end
  end
end
if calc_hess == 1
  H_g = cell(rep,K);
  for i=1:K
    np = size(gauss_obj.obj{i}.P,1);
    for rep = 1:nrep
      H_g{rep,i} = zeros(np,np);
    end
  end
end

X = cell(rep,K);

for loop=1:iter  	
	if calc_grad == 1 && calc_hess == 1
    [EX,~,alpha_sim, Nabla_sim, trace_obj, H_sim] = lgfm_sim_gauss(z, gauss_obj, trace_obj,obs_ind);
  elseif calc_grad == 1
    [EX,~,alpha_sim, Nabla_sim, trace_obj] = lgfm_sim_gauss(z, gauss_obj, trace_obj,obs_ind);
  else
    [EX,~,alpha_sim] = lgfm_sim_gauss(z, gauss_obj, trace_obj,obs_ind);
  end

	for rep = 1:nrep
		alpha = log(a0{rep})+alpha_sim{rep};
		alpha = make_K_im(alpha,[mn{rep},K]);

		for k=1:K
      if calc_grad == 1
        if ~isempty(Nabla_sim{rep,k})
          Nabla_g{rep,k} = Nabla_g{rep,k} + Nabla_sim{rep,k}/iter;
        end
      end
      if calc_hess == 1
        if ~isempty(H_sim{rep,k})
          H_g{rep,k} = H_g{rep,k} + H_sim{rep,k}/iter; 
        end
      end
			if loop==1
				X{rep,k} = EX{rep,k}/iter;
			else
				X{rep,k} = X{rep,k}+EX{rep,k}/iter;
			end
    end
		for k=1:2
      ijmask = ijm{rep}(:,k);
      f = Wij{rep,k}*z{rep};
      efb = exp(f.*b0ij{rep,k});
			Mz_cond = exp(alpha(ijmask,:)).*efb;
			Msum = sum(Mz_cond,2);
			Mz_cond = bsxfun(@rdivide,Mz_cond, Msum);
			if nargout >1
				Mzc{rep,k} = Mzc{rep,k}+Mz_cond;
			end

			if calc_grad == 1
        % Calculate gradients
        expterm = ea0ij{rep,k}(oind{rep,k},:).*efb(oind{rep,k},:);
				expsum = sum(expterm,2);
        expterm = bsxfun(@rdivide,expterm,expsum);
        esum = sum(expterm);
        fo = f(oind{rep,k},:);
        fe = fo.*expterm;
        efsum = sum(fe);
        zij = z{rep}(ijmask,:);
        zij = zij(oind{rep,k},:);
        db = db + sum(fo.*zij)- efsum;
      end
      if calc_hess == 1
        %calculate hessian
        if opt(1) ==1
          da = da + sum(zij) - esum;
          d2a = -diag(esum) + expterm'*expterm;
        end
    
        if opt(2)==1
          d2b = -diag(sum(fo.*fe)) + fe'*fe;
          dab = -diag(efsum) + fe'*expterm;
        else
          efsum2 = sum(fe,2);
          d2b = sum(efsum2.^2 - sum(fo.*fe,2));
          dab = -efsum + sum(expterm.*efsum2(:,on));
        end
        if opt(1) == 1
          H = H -[d2a dab';dab d2b];
        else
          H = H - d2b;
        end
      end
      e = rand(sum(ijmask),1);
      x = 1 + sum( bsxfun(@lt,cumsum(Mz_cond,2),e), 2);
			z{rep}(ijmask,:) = bsxfun(@eq,x,KKij{rep,k})*1;  
		end
	end
end

Mz = cell(nrep,1);
for rep = 1:nrep
	Mz{rep} = zeros(mn{rep},K);
	for k=1:2
		Mz{rep}(ijm{rep}(:,k),:) = Mzc{rep,k}/iter;
	end
end
if calc_grad == 1
  ng = Nabla_g;
  Nabla_g = cell(1,K);
  for i=1:K;
    Nabla_g{i} = zeros(np,1);
	
    for rep = 1:nrep
      Nabla_g{i} = Nabla_g{i} + ng{rep,i}; 
    end
  end
  db = db/iter;
  da = da/iter;
end

if calc_hess == 1
  nh = H_g;
  H_g = cell(1,K);

  for i=1:K;
    H_g{i} = zeros(np,np);
    for rep = 1:nrep
      H_g{i} = H_g{i} + nh{rep,i}; 
    end
  end
  H = H/iter;
  if opt(2)==0
    db = sum(db);  	 
  end
end

  
function im=make_K_im(v,sz)
	if (length(v)==1) %v = 1x1
  		im = v*ones(sz);
	elseif ((size(v,1)==sz(1)) && (size(v,2)==sz(2))) %v=mn x K, do nothing
    	im = v;
	else %v = 1xK
  		im = repmat(v,[sz(1),1]);
	end

function [EX,VX] = lgfm_gaussian_exp(fnbr,z,g_obj)
%calculates gaussian mean and variance


zi = z(:,fnbr)==1;
Y = g_obj.Y(zi,:);
A = g_obj.A(zi,:);
B = g_obj.B(zi,:);
Qd = g_obj.obj{fnbr}.Qd;
nd = size(Y,1);
n = size(g_obj.C,1);
d = size(Qd,1);
dy = size(Y,2);

AtA = A'*A;
Yk = Y; Y = Y(:);
B = kron(eye(dy),B);

kp2 = g_obj.obj{fnbr}.kp2;
sigma2 = g_obj.obj{fnbr}.sigma2;
Rd = chol(Qd);
theta = Rd(g_obj.trilind);
theta(g_obj.diagonalind) = log(theta(g_obj.diagonalind));


if g_obj.car_order == 2
  K = g_obj.G+kp2*g_obj.C;
  Qk = K'*g_obj.Ci*K;%/(4*pi*kp2);
else %CAR1 uses cholesky
  %K = chol(g_obj.G+kp2*g_obj.C);
  Qk = g_obj.G+kp2*g_obj.C;
end
Q = kron(Qd,Qk);

Qsig = sparse(diag(1./sigma2));
Qh_p = Q + kron(Qsig,AtA);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
reo = g_obj.obj{fnbr}.reo;
ireo = g_obj.obj{fnbr}.ireo;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
reo = amd(Qh_p);
ireo(reo) = 1:length(reo);

Qh_p = Qh_p(reo,reo);

%Calculate the mean
R = chol(Qh_p);
QA =kron(Qsig,A');
b = QA(reo,:)*(Y-B*g_obj.obj{fnbr}.mu);
EX = R\(R'\b);
VX = diag(Qinv(R));
EX = EX(ireo);
VX = VX(ireo);



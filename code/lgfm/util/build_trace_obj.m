function trace_obj = build_trace_obj(n_gauss,d,K,N);

if iscell(n_gauss)
	nrep = length(n_gauss);
else
	nrep = 1;
	n_gauss = {n_gauss};
end
trace_obj = cell(nrep,K);
for rep = 1:nrep
  U = zeros(n_gauss{rep}*d,N);
  Uk = cell(1,N);
  for ii=1:N
    U(:,ii) = 1-2*round(rand(n_gauss{rep}*d,1));
    Uk{ii} = reshape(U(:,ii),[n_gauss{rep} d]);
  end
  for i=1:K
    trace_obj{rep,i}.U = U;
    trace_obj{rep,i}.Uk = Uk;
    trace_obj{rep,i}.QU = U;
    trace_obj{rep,i}.QUk = Uk;
  end
  
end
function [EX,VX,alpha_sim, Nabla, trace_obj, H] = lgfm_sim_gauss(z, gaussian_obj, trace_obj,obs_ind,cv)
% Simupates the Gaussian fields in the LGFM model as well as the gradients
% and Hessian of the likelihood, conditionally on a simulation z from the
% MRF.
%Inputs:
% z : The MRF simulations
% gaussian_obj : A structure containing the matrices for the Gaussia nfields.
% trace_obj : A structure containing the matrices for the trace approximation.
% obs_ind : A vector with the indices that are observed. 
%
%
if nargin < 5
  cv = 0; %do not compute variances
  VX = [];
end
if nargin < 4;
	obs_ind = [];
end

calc_grad = 0;
if nargout > 3
  calc_grad = 1;
end
calc_hess = 0;
if nargout > 4
  calc_hess = 1;
end

if iscell(z);
	nrep = length(z);
	K = size(z{1},2);
	Nabla = cell(nrep,K);
	H = cell(nrep,K);
	EX = cell(nrep,K);
  X = cell(nrep,K);
	alpha_sim = cell(1,nrep);
	for rep = 1:nrep
		K = size(z{rep},2);
		g_obj = gaussian_obj;
		if iscell(gaussian_obj.C); g_obj.C =gaussian_obj.C{rep}; end
		if iscell(gaussian_obj.G); g_obj.G =gaussian_obj.G{rep}; end
		if iscell(gaussian_obj.A); g_obj.A =gaussian_obj.A{rep}; end
		if iscell(gaussian_obj.Ci); g_obj.Ci = gaussian_obj.Ci{rep}; end
		if iscell(gaussian_obj.Y); g_obj.Y = gaussian_obj.Y{rep}; end
		if iscell(gaussian_obj.B); g_obj.B = gaussian_obj.B{rep}; end

		for i = 1:K
			if iscell(gaussian_obj.obj{i}.Q)
				g_obj.obj{i}.Q = gaussian_obj.obj{i}.Q{rep};
			end
			if iscell(gaussian_obj.obj{i}.reo_Q)
				g_obj.obj{i}.reo_Q = gaussian_obj.obj{i}.reo_Q{rep};
			end
			if iscell(gaussian_obj.obj{i}.reo)
				g_obj.obj{i}.reo = gaussian_obj.obj{i}.reo{rep};
			end
			if iscell(gaussian_obj.obj{i}.ireo)
				g_obj.obj{i}.ireo = gaussian_obj.obj{i}.ireo{rep};
			end
			if iscell(gaussian_obj.obj{i}.R)
				g_obj.obj{i}.R = gaussian_obj.obj{i}.R{rep};
			end
			if iscell(gaussian_obj.obj{i}.EX)
				g_obj.obj{i}.EX = gaussian_obj.obj{i}.EX{rep};
			end
			if iscell(gaussian_obj.obj{i}.trKiC)
				g_obj.obj{i}.trKiC = gaussian_obj.obj{i}.trKiC{rep};
			end
			if iscell(gaussian_obj.obj{i}.trKiCKiC)
				g_obj.obj{i}.trKiCKiC = gaussian_obj.obj{i}.trKiCKiC{rep};
			end
		end

		slike = zeros(size(z{rep}));
		n = size(g_obj.C,1);
		d = size(g_obj.obj{1}.Qd,1);
		Y = gaussian_obj.Y{rep};

		for i= 1:K
			if isempty(obs_ind)
				zk = z{rep};
			else
				zk = z{rep}(obs_ind{rep},:);
      end
      if calc_grad == 1 && calc_hess == 1
        [X{rep,i}, EX{rep,i}, Nabla{rep,i}, trace_obj{rep,i},H{rep,i}] = lgfm_gaussian_grad(i,zk,g_obj,trace_obj{rep,i});
      elseif calc_grad == 1
        [X{rep,i}, EX{rep,i}, Nabla{rep,i}, trace_obj{rep,i}] = lgfm_gaussian_grad(i,zk,g_obj,trace_obj{rep,i});
      else 
        [X{rep,i}, EX{rep,i}] = lgfm_gaussian_grad(i,zk,g_obj,trace_obj{rep,i});
      end
      if cv==1
        [~,VX{rep,i}] = lgfm_gaussian_exp(i,zk,g_obj);
      end
			sigma2 = g_obj.obj{i}.sigma2;
      %given simulated xi, calculate Y-B*mu-A*xi
      Ydiff = Y - repmat(g_obj.B,[1 d])*diag(g_obj.obj{i}.mu);
      Ydiff = Ydiff - g_obj.A*reshape(X{rep,i},[n d]);

      %likelihood of Ydiff is N(0,diag(sigma_e))
      like = -d*log(2*pi)-sum(log(sigma2))-(Ydiff*diag(1./sigma2)).*Ydiff;
      
      sl(:,i) = sum(like/2,2);
    end
    if isempty(obs_ind)
      slike = sl; 
    else
      slike(obs_ind{rep},:) = sl;
    end
		alpha_sim{rep} = slike;
	end
else
	K = size(z,2);
	alpha_sim = zeros(size(z));
	n = size(gaussian_obj.C,1);
	d = size(gaussian_obj.obj{1}.Qd,1);
	Y = gaussian_obj.Y;
	
	Nabla = cell(1,K);
	H = cell(1,K);
	EX = cell(1,K);
  X = cell(1,K);
	for i= 1:K
		if isempty(obs_ind)
			zk = z;
		else
			zk = z(obs_ind,:);
    end
    
    if calc_grad == 1 && calc_hess == 1
      [X{i}, EX{i}, Nabla{i}, trace_obj{i},H{i}] = lgfm_gaussian_grad(i,zk,gaussian_obj,trace_obj{i});
    elseif calc_grad == 1
      [X{i}, EX{i}, Nabla{i}, trace_obj{i}] = lgfm_gaussian_grad(i,zk,gaussian_obj,trace_obj{i});
    else 
      [X{i}, EX{i}] = lgfm_gaussian_grad(i,zk,gaussian_obj,trace_obj{i});
    end
        
		sigma2 = gaussian_obj.obj{i}.sigma2;
		
    %given simulated xi, calculate Y-B*mu-A*xi
    Ydiff = Y - repmat(gaussian_obj.B,[1 d])*...
 	   					diag(gaussian_obj.obj{i}.mu);
    Ydiff = Ydiff - gaussian_obj.A*reshape(X{i},[n d]);

    %likelihood of Ydiff is N(0,diag(sigma_e))
    like = -d*log(2*pi)-sum(log(sigma2)) ...
    				-(Ydiff*diag(1./sigma2)).*Ydiff;
    
    alpha_sim(:,i) = sum(like/2,2);
  end
end



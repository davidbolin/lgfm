function gaussian_obj = build_gauss_obj(z,G,C,A,Y,theta,kp0,sig0,car_order)
if nargin < 7; kp0 = 1; end
if nargin < 8; sig0 = 1; end

if iscell(z);
	nrep = length(z);
else
	nrep = 1;
	Y  = {Y};	C = {C};
	G = {G};	A = {A};
	z = {z}
end

gaussian_obj = struct;
for rep=1:nrep
	[nd{rep} d] = size(Y{rep});
	K = size(z{rep},2);
	n{rep} = size(C{rep},1);
	Ci{rep} = inv(C{rep});
	gaussian_obj.B{rep} = ones(nd{rep},1);
end

gaussian_obj.G = G;
gaussian_obj.C = C;
gaussian_obj.Ci = Ci;
gaussian_obj.Y = Y;
gaussian_obj.A = A;
gaussian_obj.car_order = car_order;
tmp = ones(d,d);
ttmp = triu(tmp);
gaussian_obj.trilind = find(ttmp);
gaussian_obj.diagind = find(eye(d));
gaussian_obj.diagonalind = cumsum(1:d);

gaussian_obj.obj = cell(K,1);

for rep=1:nrep
  if car_order == 1
    Qk{rep} = G{rep} + kp0*C{rep};
  else
  	Qk{rep} = (G{rep} + kp0*C{rep})*Ci{rep}*(G{rep}+kp0*C{rep});
  end
end

for i=1:K
	Qd = inv(theta{i}.Sigma);
	[qtmp p] = chol(Qd);
	if p>0;
		Qd = inv(theta{i}.Sigma+1e-5*eye(size(tmp)));
	end
	Rd = chol(Qd);
	theta0 = Rd(gaussian_obj.trilind);
	theta0(cumsum(1:d)) = log(theta0(cumsum(1:d)));

	gaussian_obj.obj{i}.Qd = Qd;
	gaussian_obj.obj{i}.opts = [];
	gaussian_obj.obj{i}.mu = theta{i}.mu';
	gaussian_obj.obj{i}.kp2 = kp0;
	if length(sig0) == 1
		gaussian_obj.obj{i}.sigma2 = sig0*ones(d,1);
		sigma0 = sig0*ones(d,1);
	else
		gaussian_obj.obj{i}.sigma2 = sig0;
		sigma0 = sig0;
	end
	for rep = 1:nrep
		gaussian_obj.obj{i}.Q{rep} = kron(Qd,Qk{rep});
		gaussian_obj.obj{i}.reo_Q{rep} = amd(Qk{rep});
		reo = amd(gaussian_obj.obj{i}.Q{rep});
		gaussian_obj.obj{i}.reo{rep} = reo;
		gaussian_obj.obj{i}.ireo{rep}(reo) = 1:(d*n{rep});
    if car_order == 1
  		gaussian_obj.obj{i}.R{rep} = kron(chol(Qd),chol(G{rep} + kp0*C{rep}));
    else
  		gaussian_obj.obj{i}.R{rep} = kron(chol(Qd),(G{rep} + kp0*C{rep})'*sqrt(Ci{rep}));
  		end
	end

	P = [theta{i}.mu';log(sigma0);log(kp0);theta0];
	gaussian_obj.obj{i}.P = P;

end

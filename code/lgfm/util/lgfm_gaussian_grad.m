function [sim, EX, Nabla, trace_obj,H] = lgfm_gaussian_grad(fnbr,z,g_obj,trace_obj)
%calculates gaussian simulation, mean, grandient and hessian given a MRF
%sample for the
% Inputs:
% fnbr  : the class number to simulate the Gaussian field for
% z     : the simulated MRF
% g_obj : a structure containing the matrices for the Gaussian fields
% trace_obj : a structure containing the matrices for the trace
%             approximations.
%
% Outputs:
% sim : The simulated Gaussian field
% Nabla : The gradient of the likelihood
% H    : The hessian of the liklihood
% trace_obj : the updated trace object.
% X : The expected value of the Gaussian field

calcgrads = 0;
if nargout > 2
  calcgrads = 1;
end

use_hess = 0;
if nargout > 4;
  use_hess = 1;
end

zi = z(:,fnbr)==1;
if sum(zi)==0
  %disp('No observations for one of the classes. Cannot update parameters.')
  calcgrads = 0;
end
Y = g_obj.Y(zi,:);
A = g_obj.A(zi,:);
B = g_obj.B(zi,:);
Qd = g_obj.obj{fnbr}.Qd;
nd = size(Y,1);
n = size(g_obj.C,1);
d = size(Qd,1);
dy = size(Y,2);

AtA = A'*A;
Yk = Y; Y = Y(:);
B = kron(eye(dy),B);

kp2 = g_obj.obj{fnbr}.kp2;
sigma2 = g_obj.obj{fnbr}.sigma2;
Rd = chol(Qd);
theta = Rd(g_obj.trilind);
theta(g_obj.diagonalind) = log(theta(g_obj.diagonalind));


if g_obj.car_order == 2
  K = g_obj.G+kp2*g_obj.C;
  Qk = K'*g_obj.Ci*K;
else %CAR1 uses cholesky
  %K = chol(g_obj.G+kp2*g_obj.C);
  Qk = g_obj.G+kp2*g_obj.C;
end
Q = kron(Qd,Qk);


Qsig = sparse(diag(1./sigma2));
Qh_p = Q + kron(Qsig,AtA);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
reo = g_obj.obj{fnbr}.reo;
ireo = g_obj.obj{fnbr}.ireo;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%reo = amd(Qh_p);
%ireo(reo) = 1:length(reo);

Qh_p = Qh_p(reo,reo);

%Calculate preconditioner
if g_obj.car_order == 1
  M1 = chol(Qh_p,'lower');
else
  M1 = RIC(Qh_p,1e-5,0.2);
end
M2 = M1';
%Calculate the mean
QA =kron(Qsig,A');
b = QA(reo,:)*(Y-B*g_obj.obj{fnbr}.mu);
[Z, flag, relres] = pcg(Qh_p,b,1e-3,100,M1,M2,g_obj.obj{fnbr}.EX(reo));
X = Z(ireo);
Xk = reshape(X,[n d]);

if flag ~= 0
	disp(' ')
    disp('Warning pcg in the mean has not converged')
    disp('in lmrf_gaussian_grad2')
    disp(['Relative error = ', num2str(relres)])
    disp(['flag = ', num2str(flag)])
end

%Make a draw from the posterior
Qsig(1) = 1/sigma2(1);
R_Qsig = kron(sqrt(Qsig),A');
if g_obj.car_order == 1
  Rk = M2;%chol(Qh_p);
  sim = Rk\randn(size(Rk,1),1);
  sim = sim(ireo) + X;
  %R_Q = kron(Rd',K');
else
  R_Q = kron(Rd',K');
  sim = R_Q*randn(d*n,1) + R_Qsig*randn(nd*d,1);
  [sim flag,relres] = pcg(Qh_p,sim(reo),1e-3,1000,M1,M2,Z);
  sim = sim(ireo) + X;
end

if calcgrads
	if flag ~= 0
     disp(' ')
     disp('Warning pcg in the draw poseterior has not converged')
     disp('in lmrf_gaussian_grad')
     disp(['Relative error = ', num2str(relres)])
     disp(['flag = ', num2str(flag)])
	end

	%Update traces
	N = size(trace_obj.U,2);
	U = trace_obj.U;
	QU = trace_obj.QU;
	Uk = trace_obj.Uk;
	QUk = trace_obj.QUk;
  UR = U(reo,:);
  QUR = QU(reo,:);
  for ii=1:N
    [QUi, flag, relres] = pcg(Qh_p, UR(:,ii), ...
        1e-3, 100, M1, M2, QUR(:,ii));

    if flag ~= 0
      disp(' ')
      disp('Warning pcg in the trace estimation has not converged')
      disp('in lmrf_gaussian_grad')
      disp(['Relative error = ', num2str(relres)])
      disp(['flag = ', num2str(flag)])
    end
    QU(:,ii) = QUi(ireo);
    QUk{ii} = reshape(QU(:,ii),[n d]);
  end
  trace_obj.QU = QU;
  trace_obj.U = U;
  trace_obj.QUk = QUk;
  trace_obj.Uk = Uk;


	%Calculate gradient and approximate hessian:

	%\sigma and \mu %
	dmu=zeros(d,1); dsigma = zeros(d,1); d2mu = zeros(d,1);
	d2sigma = zeros(d,1); dsigmadmu = zeros(d,1);
	for k=1:d
		diffk = Yk(:,k)-A*Xk(:,k)-g_obj.obj{fnbr}.mu(k);
		dmu(k) = sum(diffk)/sigma2(k);
		tmp = sparse(d,d);
		tmp(k,k) = -1/sigma2(k);
		tmp = kron(tmp,AtA);
		trest = sum(diag(U'*tmp*QU))/N;
    dsigma(k)= (-nd + diffk'*diffk/sigma2(k)-trest)/2;

    %hessian
    if use_hess
      d2sigma(k) = (-diffk'*diffk/sigma2(k) + trest)/2;
      dsigmadmu(k) = -dmu(k);
      d2mu(k) = -nd/sigma2(k);
    end
	end

	%\kappa and \theta %
	d2theta = zeros(d*(d+1)/2, d*(d+1)/2);
	dthetadkappa = zeros(d*(d+1)/2,1);
	dtheta = zeros(d*(d+1)/2,1);

	trestG = 0;
	trestC = 0;
	trestQ = 0;
	for ii=1:N
    trestG = trestG + sum(sum(Uk{ii}.*(g_obj.G*QUk{ii}*Qd)));
    trestC = trestC + sum(sum(Uk{ii}.*(g_obj.C*QUk{ii}*Qd)));
    trestQ = trestQ + sum(sum(Uk{ii}.*(Qk*QUk{ii}*Qd)));
	end
	trestG = trestG/N;
	trestC = trestC/N;
	trestQ = trestQ/N;

  if g_obj.car_order == 1
    % Add d*tr(Q^-1*dQ)
    dkappa = 0.5*d*kp2*g_obj.obj{fnbr}.trKiC;
    % add quadratic form
    dkappa = dkappa -0.5*kp2*sum(sum(Xk.*(g_obj.C*Xk*Qd)));
    %add final trace
    dkappa = dkappa - 0.5*kp2*trestC;

%    dkappa = kp2*(d*g_obj.obj{fnbr}.trKiC - sum(sum(Xk.*(g_obj.C*Xk*Qd)))-trestC)/2; %was without/2
    %hessian
    if use_hess
      d2kappa= dkappa-d*kp2^2*g_obj.obj{fnbr}.trKiCKiC;
    end
  else
    %add d*tr(Q^-1*dQ)
    dkappa = kp2*d*g_obj.obj{fnbr}.trKiC;
    %add quadratic form
    dkappa = dkappa -kp2*sum(sum(Xk.*(K*Xk*Qd)));
    %add final trace
    dkappa = dkappa - kp2*(trestG+kp2*trestC);

%    dkappa = - d*n/2 + kp2*d*g_obj.obj{fnbr}.trKiC ...
 %   		 -sum(sum(Xk.*(K*Xk*Qd)))/(4*pi) + sum(sum(Xk.*(Qk*Xk*Qd)))/2 ...
  %  		 - (trestG+kp2*trestC)/(4*pi) + trestQ/2;
    %hessian
    if use_hess
      d2kappa = dkappa;
      d2kappa = d2kappa + kp2*(d*kp2*g_obj.obj{fnbr}.trKiCKiC);
      d2kappa = d2kappa - kp2*sum(sum(Xk.*(g_obj.C*Xk*Qd)));
      d2kappa = d2kappa - kp2*trestC;
    %  d2kappa= kp2*d*g_obj.obj{fnbr}.trKiC ...
     %           -d*kp2^2*g_obj.obj{fnbr}.trKiCKiC ...
      %          - sum(sum(Xk.*(Qk*Xk*Qd)))/2 ...
       %         + sum(sum(Xk.*(K*Xk*Qd)))/(4*pi) ...
        %        - kp2*sum(sum(Xk.*(g_obj.C*Xk*Qd)))/(4*pi) ...
         %       - trestQ/2 - kp2*trestC/(4*pi) + (trestG+kp2*trestC)/(4*pi);
    end
  end
  %d\theta
	for k=1:d*(d+1)/2
    dkR = sparse(d,d); dkR(g_obj.trilind(k)) = 1;
    dlogdet = 0;
    if sum(g_obj.trilind(k)==g_obj.diagind)>0
      dkR(g_obj.trilind(k)) = exp(theta(k));
      dlogdet = 1;
    end
    dQd = sparse(dkR'*Rd + Rd'*dkR);
    trest = 0;
    trest2 = 0;
    for ii=1:N
      trest = trest + sum(sum(Uk{ii}.*(Qk*QUk{ii}*dQd)));
      if g_obj.car_order == 1
        trest2 = trest2 + sum(sum(Uk{ii}.*(g_obj.C*QUk{ii}*dQd)));
      else
        trest2 = trest2 + sum(sum(Uk{ii}.*(K*QUk{ii}*dQd)));
      end
    end
    trest = trest/N;
    trest2 = trest2/N;
    dtheta(k) = (n*dlogdet - sum(sum(Xk.*(Qk*Xk*dQd)))/2 - trest/2);

    %hessian
    if use_hess
      if g_obj.car_order == 1
        dthetadkappa(k)=-kp2*(sum(sum(Xk.*(g_obj.C*Xk*dQd))) + trest2)/2;
      else
      dthetadkappa(k) = -kp2*(sum(sum(Xk.*(K*Xk*dQd))) + trest2);
        %dthetadkappa(k) = -sum(sum(Xk.*(K*Xk*dQd)))/(4*pi) ...
   	 		%					+ sum(sum(Xk.*(Qk*Xk*dQd)))/2 ...
   	 	%						+ trest/2 - trest2/(4*pi);
      end
      for l=k:d*(d+1)/2
        d2Qd = 0;
        dlR = sparse(d,d); dlR(g_obj.trilind(l)) = 1;
        if sum(g_obj.trilind(l)==g_obj.diagind)>0
          dlR(g_obj.trilind(l)) = exp(theta(l));
          if k==l
            d2Qd = dQd;
          end
        end
        d2Qd = sparse(d2Qd + dkR'*dlR + dlR'*dkR);
        trest = 0;
        if sum(abs(d2Qd(:)))>0
          for ii=1:N
            trest = trest + sum(sum(Uk{ii}.*(Qk*QUk{ii}*d2Qd)));
          end
          trest = trest/N;
        end
        d2theta(k,l) = -sum(sum(Xk.*(Qk*Xk*d2Qd)))/2 -trest/2;
        d2theta(l,k) = d2theta(k,l);
      end
		end
	end

	Nabla = [dmu;dsigma;dkappa;dtheta];

	if use_hess
   	H = [diag(d2mu)         diag(dsigmadmu)     zeros(d,1)    zeros(d,d*(d+1)/2);
       	 diag(dsigmadmu)    diag(d2sigma)       zeros(d,1)    zeros(d,d*(d+1)/2);
         zeros(1,d)         zeros(1,d)          d2kappa       dthetadkappa';
         zeros(d*(d+1)/2,d) zeros(d*(d+1)/2,d)  dthetadkappa  d2theta];
	end
else
	H = [];
	Nabla = [];
end
EX = X;
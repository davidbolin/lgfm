function trace_obj = update_trace_obj(trace_obj)

for rep = 1:size(trace_obj,1);
  N = size(trace_obj{1,1}.U,2);
  i_up = randperm(N); 
  i_up = i_up(1);
  U = trace_obj{rep,1}.U;
  Uk = trace_obj{rep,1}.Uk;
  U(:,i_up) = 1-2*round(rand(size(U,1),1));
  Uk{i_up} = reshape(U(:,i_up),size(Uk{i_up}));
  for i=1:size(trace_obj,2)
    trace_obj{rep,i}.U = U;
    trace_obj{rep,i}.Uk = Uk;
    trace_obj{rep,i}.QU = U;
    trace_obj{rep,i}.QUk = Uk;
  end
  
end
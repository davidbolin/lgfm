function updated_obj = lgfm_update_gauss(P,fnbr,gaussian_obj,EX)

updated_obj = gaussian_obj.obj{fnbr};
updated_obj.P = P;

d = size(gaussian_obj.obj{fnbr}.Qd,1);

beta_mu = P(1:d);
logsigma2 = P(d+1:2*d);
%limit kappa for numerical stability. (so that ranges are reasonable)
if gaussian_obj.car_order == 1
  logkp2 = min(max(P(2*d+1),log(1e-16)),log(8/0.1^2));
else
  logkp2 = min(max(P(2*d+1),log(1e-8)),log(8/0.1^2));
end
theta = P(2*d+2:end);
Rd = sparse(d,d);
Rd(gaussian_obj.trilind) = theta;
%limit diagonal so that variances are reasonable.
Rd(gaussian_obj.diagind) = min(max(exp(Rd(gaussian_obj.diagind)),1e-8),1e8);
Qd = full(Rd'*Rd);
kp2 = exp(logkp2);
sigma2 = exp(logsigma2);

updated_obj.kp2 = kp2;
updated_obj.sigma2 = sigma2;
updated_obj.mu = beta_mu;
updated_obj.Qd = Qd;

nrep = length(gaussian_obj.G);
for rep = 1:nrep
  K = (gaussian_obj.G{rep} + kp2*gaussian_obj.C{rep});
  if gaussian_obj.car_order == 1
    Qk = K;
  else
    Qk = K'*gaussian_obj.Ci{rep}*K;
  end
  updated_obj.EX{rep} = EX{rep}(:);

  reo_Q = gaussian_obj.obj{fnbr}.reo_Q{rep};
  R_Q = chol(Qk(reo_Q,reo_Q));
  updated_obj.Qi{rep} = Qinv(R_Q);
  if gaussian_obj.car_order == 1
    KiC = updated_obj.Qi{rep}*gaussian_obj.C{rep}(reo_Q,reo_Q);
    KiCKiC = KiC.*KiC;
    updated_obj.trKiC{rep} = sum(diag(KiC));
    updated_obj.trKiCKiC{rep} = sum(sum(KiCKiC));
  else
    KiC = updated_obj.Qi{rep}.*K(reo_Q,reo_Q);
    KiCKiC = updated_obj.Qi{rep}.*gaussian_obj.C{rep}(reo_Q,reo_Q);
    updated_obj.trKiC{rep} = sum(sum(KiC));
    updated_obj.trKiCKiC{rep} = sum(sum(KiCKiC));
  end
end
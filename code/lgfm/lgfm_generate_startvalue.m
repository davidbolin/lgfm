function [gauss_obj, z0] = lgfm_generate_startvalue(Y, mask, obs_ind,K, opts,iter)
%Inputs:
% Y       : data
% mask    : the mask
% obs_in  : the indices for the observation locations. 
% K       : The number of classes to use.
% opts    : structure with options
% iter    : number of iterations in the initial optimization

if nargin < 6; iter = 30; end

if iscell(Y);
	nrep = length(Y);
  if ~iscell(mask) || ~iscell(obs_ind)
    error('mask and obs_ind should be cells if Y is.')
  end
else
	nrep = 1;
  Y  = {Y}; 
  mask = {mask};
  obs_ind = {obs_ind};
end

ystack = [];
for rep = 1:nrep
  ystack = [ystack;Y{rep}];
end
disp('Compute initial classification using EM algorithm.')
[theta,~, pstack,ll] = normmix_em(ystack,K,[],0);
for i=1:10 %repeat 10 times and choose classification with highest likelihood.
  [theta_new,~, pstack_new,ll_new] = normmix_em(ystack,K,2,0);
  if ll_new>ll
    theta = theta_new;
    pstack = pstack_new;
    ll = ll_new;
  end
end


[tmp,cl]=max(pstack,[],2);
for rep = 1:nrep;
	nd = size(Y{rep},1);
	clk = cl(1:nd);
	cl = cl(nd+1:end);
	z{rep} = zeros(nd,K);
	for i=1:nd; z{rep}(i,clk(i))=1; end
  z0{rep} = zeros(sum(mask{rep}(:)),K);
  for i=1:nd
    z0{rep}(obs_ind{rep}(i),:) = z{rep}(i,:); 
  end
  %draw random starting values for z at unobserved pixels
  for i=1:size(z0{rep},1)
    if sum(z0{rep}(i,:))==0; z0{rep}(i,randi(K,1)) = 1; end
  end
end

kp0 = 8/(0.5*size(mask{rep},1))^2; %starting value for kappa

gauss_obj = lgfm_generate_startvalue_gauss(z,Y,mask,obs_ind,theta,iter,kp0,opts);



function gauss_obj = lgfm_generate_startvalue_gauss(z,Y,mask,obs_ind,theta,iter,kp0,opts)

if iscell(Y);
	nrep = length(Y);
else
	nrep = 1;
	Y  = {Y};	
	z = {z};
end

%build CAR matrices:
C = cell(1,nrep);
G = cell(1,nrep);
A = cell(1,nrep);
for i=1:nrep
  G{i} = stencil2prec(mask{i},[0 -1 0;-1 4 -1;0 -1 0]);
  C{i} = speye(numel(mask{i}));
  C{i} = C{i}(mask{i}(:),mask{i}(:));
  A{i} = C{i}(obs_ind{i},:);
end


gauss_obj = build_gauss_obj(z,G,C,A,Y,theta,kp0,1,opts.car_order);
for rep = 1:nrep
	[mn{rep} K] = size(z{rep});
end

d = size(Y{1},2);
obj = cell(1,K);
for kk=1:K
  for rep = 1:nrep
    ik = z{rep}(:,kk)==1;
		Yest{rep} = Y{rep}(ik,:);
		Aest{rep} = A{rep}(ik,:);
    
    obs_ind_est{rep} = obs_ind{rep}(ik);
	end
	p0 = struct;
	p0.mu0 = theta{kk}.mu';
	Qd = inv(theta{kk}.Sigma);
	p0.Qd = Qd;
	p0.logkappa2 = log(kp0);
	p0.logsigma2 = log(var(Yest{rep})/100)';

	[gtheta, Xk] = gaussian_ecg(Yest,mask,obs_ind_est,p0,opts,iter);
	obj{kk} = lgfm_update_gauss(gtheta.P,kk,gauss_obj,Xk);
end
gauss_obj.obj = obj;